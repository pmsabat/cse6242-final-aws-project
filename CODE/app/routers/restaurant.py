from fastapi import Depends, APIRouter, status, HTTPException
from typing import Any, List, Dict, Optional, Sized, Union
from app.controllers.restaurant import restaurantsController
from app.models.connection import engine, SessionLocal, get_db
from app.controllers.restaurant import *
from sqlalchemy.orm import Session
from app.models.restaurants import RestaurantDict, ExactRestaurantDict, ExactRestaurantsDict

restaurant_router = APIRouter()
restaurant_controller = restaurantsController()

# TODO: should have a response type like: @app.get("/cats/{id}", response_model=Cats) define the response type
# for all routes!


@restaurant_router.get("/restaurants/", response_model=ExactRestaurantsDict)
async def get_restaurants(
    lat: Optional[float] = None,
    lon: Optional[float] = None,
    limit: int = 5,
    food_type: Optional[str] = "American",
    food_price: Optional[str] = "$",
) -> ExactRestaurantsDict:
    # TODO: updated to use take args as needed and get data from controllers
    print(f"lat was: {lat}, lon was: {lon}")
    result: ExactRestaurantsDict = (
        restaurant_controller.get_all_advanced(
            coordinate=(lat, lon), limit=limit, food_type=food_type, food_price=len(food_price)  # type: ignore
        )
        if lat is not None and lon is not None
        else restaurant_controller.get_all_advanced(limit=limit)
    )

    for i in result:
        print("The result was: {}, {}, {}".format(i["name"], i["latitude"], i["longitude"]))
        print(type(i))
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    return result


# @restaurant_router.get("/restaurants/{location_id}")
# async def get_restaurant(location_id: str) -> Dict[str, Union[str, int, float]]:
#     # TODO: updated to use take args as needed and get/add data from controllers
#
#     post, stat = (None, None)
#     if not stat:
#         raise HTTPException(
#             status_code=status.HTTP_404_NOT_FOUND, detail=f"Restaurant with id: {location_id} not found"
#         )
#     return {"data": post}


# @restaurant_router.get("/filter")
# async def filter_data(
#     price_min: int = 0, price_max: int = 4, cuisines: str = "", rating: float = 0.0
# ) -> Dict[str, List[Dict[str, Union[str, float, int]]]]:
#     input: Dict[str, Union[str, int, float]] = {
#         "price_min": price_min,
#         "price_max": price_max,
#         "cuisines": cuisines,
#         "rating": rating,
#     }
#     print(input)
#     # send this data to controller
#     data = restaurant_controller.get_restaurant(input)
#     return {"restaurant": "sample data for now"}

# @restaurant_router.delete("/restaurant/")
# async def delete_restaurant() -> dict[str, str]:
#     # TODO: updated to use take args as needed and delete data from controllers
#     return {"restaurant": "sample data for now"}
#
#
# @restaurant_router.put("/restaurant/")
# async def update_restaurant(id: str, post: Dict[str, Union[str, float, int]]) -> dict[str, str]:
#     # # TODO: updated to use take args as needed and get/update data from controllers
#     # # new_post = post.dict(exclude_unset=True)
#     # stat = restaurant_controller.update_post_by_id(id, post)
#     # if not stat:
#     #     raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"post with id: {id} not found")
#
#     return {"msg": "post updated"}
#
#
# @restaurant_router.get("/restaurant/{id}")
# async def get_restaurant_by_id(id: str) -> dict[str, str]:
#     # TODO: Make accept price min, price max, nights someone is staying, geolocation, etc.
#     # TODO: updated to use take args as needed and get data from controllers
#     return {"restaurant": "sample data for now"}


## Add as needed here
