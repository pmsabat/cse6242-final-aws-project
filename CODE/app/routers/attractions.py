from typing import Optional
from fastapi import APIRouter
from app.controllers.attraction import attractionsController
from app.controllers.geo import Coordinate
from app.models.attractions import AttractionDict, ExactAttractionDict, ExactAttractionsDict

attraction_router = APIRouter()
attraction_controller = attractionsController()
# TODO: should have a response type like: @app.get("/cats/{id}", response_model=Cats) define the response type
# for all routes!
@attraction_router.get("/attractions", response_model=ExactAttractionsDict)
async def get_attractions(
    lat: Optional[float] = None, lon: Optional[float] = None, limit: int = 5
) -> ExactAttractionsDict:
    # TODO: updated to use take args as needed and get data from controllers
    print(f"lat was: {lat}, lon was: {lon}")
    result: ExactAttractionsDict = (
        attraction_controller.get_all_advanced(coordinate=(lat, lon), limit=limit)
        if lat is not None and lon is not None
        else attraction_controller.get_all_advanced(limit=limit)
    )
    # print(f"The result was: {result}")
    return result


@attraction_router.post("/attraction/")
async def post_attraction() -> dict[str, str]:
    # TODO: updated to use take args as needed and get/add data from controllers
    return {"attraction": "sample data for now"}


@attraction_router.delete("/attraction/")
async def delete_attraction() -> dict[str, str]:
    # TODO: updated to use take args as needed and delete data from controllers
    return {"attraction": "sample data for now"}


@attraction_router.put("/attraction/")
async def update_attraction() -> dict[str, str]:
    # TODO: updated to use take args as needed and get/update data from controllers
    return {"attraction": "sample data for now"}


@attraction_router.get("/attraction/{id}")
async def get_attraction_by_id(id: str) -> dict[str, str]:
    # TODO: Make accept price min, price max, nights someone is staying, geolocation, etc.
    # TODO: updated to use take args as needed and get data from controllers
    return {"attraction": "sample data for now"}


## Add as needed here
