from fastapi import Depends, APIRouter, status, HTTPException
from typing import Any, List, Dict, Optional, Union
from app.controllers.airbnb import airBnbsController
from app.models.connection import engine, SessionLocal, get_db
from app.controllers.restaurant import *
from sqlalchemy.orm import Session
from app.models.airbnb import AirbnbDict, ExactAirbnbDict, ExactAirbnbsDict

airbnb_router = APIRouter()
airbnb_controller = airBnbsController()
# TODO: should have a response type like: @app.get("/cats/{id}", response_model=Cats) define the response type
# for all routes!
# @airbnb_router.get("/airbnbs/")
# async def get_airbnbs(
#     min_price: float = 0.0, max_price: float = 1000.0, min_nights: int = 1, max_nights: int = 30
# ) -> list[dict[str, str]]:
#     # TODO: updated to use take args as needed and get data from controllers
#     # airbnbs: list[dict[str, str]] = airBnbController.get_airbnbs(min_price, max_price, min_nights, max_nights)
#     pass


@airbnb_router.get("/airbnbs/", response_model=ExactAirbnbsDict)
async def get_filtered_airbnbs(
    airbnb_min_cost: Optional[int] = 10,
    airbnb_max_cost: Optional[int] = 500,
    accommodation_type: Optional[str] = "Private",
    min_nights: Optional[int] = 1,
    max_nights: Optional[int] = 3,
    guests: Optional[int] = 1,
) -> ExactAirbnbsDict:
    # TODO: updated to use take args as needed and get data from controllers
    # print(f"lat was: {lat}, lon was: {lon}")
    result: ExactAirbnbsDict = airbnb_controller.get_airbnbs(
        airbnb_min_cost=airbnb_min_cost,
        airbnb_max_cost=airbnb_max_cost,
        accommodation_type=accommodation_type,
        min_nights=min_nights,
        max_nights=max_nights,
        guests=guests,
    )

    # for key, val in result.items():
    #     print("The result was: \n{}:{}\n".format(key, val))
    # print(type(val))
    return result


# @airbnb_router.post("/airbnb/")
# async def post_airbnb() -> dict[str, str]:
#     # TODO: updated to use take args as needed and get/add data from controllers
#     return {"airbnb": "sample data for now"}
#
#
# @airbnb_router.delete("/airbnb/")
# async def delete_airbnb() -> dict[str, str]:
#     # TODO: updated to use take args as needed and delete data from controllers
#     return {"airbnb": "sample data for now"}
#
#
# @airbnb_router.put("/airbnb/")
# async def update_airbnb() -> dict[str, str]:
#     # TODO: updated to use take args as needed and get/update data from controllers
#     return {"airbnb": "sample data for now"}
#
#
# @airbnb_router.get("/airbnb/{id}")
# async def get_airbnb_by_id(id: str) -> dict[str, str]:
#     # TODO: Make accept price min, price max, nights someone is staying, geolocation, etc.
#     # TODO: updated to use take args as needed and get data from controllers
#     return {"airbnb": "sample data for now"}


## Add as needed here
