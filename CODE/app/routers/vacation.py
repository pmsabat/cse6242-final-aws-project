from typing import List
from fastapi import APIRouter
from app.controllers.airbnb import airBnbsController
from app.controllers.attraction import attractionsController
from app.controllers.restaurant import restaurantsController
from app.controllers.geo import geoController
from app.controllers.geo import FullVacationPlanned
from app.models.airbnb import AirbnbDict, ExactAirbnbDict, ExactAirbnbsDict
from app.models.attractions import AttractionDict, ExactAttractionDict, ExactAttractionsDict
from app.models.restaurants import RestaurantDict, ExactRestaurantDict, ExactRestaurantsDict

vacation_router = APIRouter()
airbnb_controller = airBnbsController()
attraction_controller = attractionsController()
restaurant_controller = restaurantsController()
geo_controller = geoController()
# TODO: should have a response type like: @app.get("/cats/{id}", response_model=Cats) define the response type
# for all routes!
@vacation_router.get("/vacations", response_model=List[FullVacationPlanned])
async def plan_vacation(
    accommodation_type: str = "Entire rental unit",
    airbnb_min_cost: float = 0.0,
    airbnb_max_cost: float = 500.0,
    guests: int = 1,
    min_nights: int = 1,
    max_nights: int = 30,
    site_category: str = "Sights & Landmarks",
    food_type: str = "American",
    food_price: str = "$",
    time_value: int = 15,
) -> List[FullVacationPlanned]:
    # TODO: updated to use take args as needed and get data from controllers
    print(
        f"I got the following params: accom_type:{accommodation_type}, airbnb_min_cost:{airbnb_min_cost}, airbnb_max_cost:{airbnb_max_cost}, guests:{guests}, min_nights:{min_nights}, max_nights:{max_nights}, site_category:{site_category}, food_type:{food_type}, food_price:{food_price}, time_value:{time_value}"
    )
    # set default limit for tops to find
    limit = 2
    # TODO: Query airbnb for airbnbs that fit per cluster
    # find the best airbnb from each cluster and return the top ones
    airbnb_result: ExactAirbnbsDict = airbnb_controller.get_airbnbs(
        airbnb_min_cost=airbnb_min_cost,
        airbnb_max_cost=airbnb_max_cost,
        accommodation_type=accommodation_type,
        min_nights=min_nights,
        max_nights=max_nights,
        guests=guests,
    )

    # Using the lat longs from the airbnbs, find the best attractions
    # attractions_result: ExactAttractionsDict = []
    # restaurants_result: ExactRestaurantsDict = []
    vacation_list: list[FullVacationPlanned] = []
    for airbnb in airbnb_result:
        lat = airbnb["latitude"]
        lon = airbnb["longitude"]

        # TODO: Query attractions per airbnb for attractions that fit criterea
        # attractions = attractionsController.get_all_advanced(cat=site_category, coordinate=(airbnb[lat], airbnb[lon]), limit=5)
        attract_result: ExactAttractionsDict = (
            attraction_controller.get_all_advanced(coordinate=(lat, lon), limit=limit, sub_cat=site_category)
            if lat is not None and lon is not None
            else attraction_controller.get_all_advanced(limit=limit)
        )
        # print(f"The result was: {result}")

        # TODO: Query restaurants per airbnb for restaurants that fit critera
        # Using the lat longs from the airbnbs, find the best attractions
        rest_result: ExactRestaurantsDict = (
            restaurant_controller.get_all_advanced(
                coordinate=(lat, lon), limit=limit, food_type=food_type, food_price=len(food_price)
            )
            if lat is not None and lon is not None
            else restaurant_controller.get_all_advanced(limit=limit, food_type=food_type, food_price=len(food_price))
        )
        # print(f"The result was: {result}")

        print(f"Results:\n Airbnb lat: {airbnb['latitude']} & airbnb lon {airbnb['longitude']}")
        for attraction in attract_result:
            print(
                f"the attraction name was: {attraction['name']} at lat - {attraction['latitude']} lon - {attraction['longitude']}"
            )
        for rest in rest_result:
            print(f"the restaurant name was: {rest['name']} at lat - {rest['latitude']} lon - {rest['longitude']}")
        # TODO: Per group (Airbnb, attractions, restaurants) Pass all to geo-controller to get TSP routing
        # TODO: Should have N solved routes, where N is number of clusters at beginning
        # TODONE: Per route - Enrich TSP solution with actual location details, JOIN by lat/lon
        # TODO: Reintroduce this MOCK CODE:
        try:
            result = await geo_controller.weighted_tsp(airbnb, time_value, attract_result, rest_result)
            # TODO: Reintroduce this MOCK CODE:
            vacation_list.append(result)
        except Exception as ex:
            print(f"One of our vacations failed to plan - {ex}")
        # TODO: Return the vacation list

    # TODO: Return best as optimization_response to frontend
    #  {"code": str, "trips": list[trip], "waypoints": list[enhanced_waypoint]}
    # Enhanced Waypoint:
    # "name": str,
    # "type": site_types,  # This tells you which key will be populated below
    # "location": list[Coordinate],
    # "trip_index": int,
    # "waypoint_index": int,
    # "airbnb": Optional[dict[str,str]],
    # "restaurant": Optional[dict[str,str]],
    # "attraction": Optional[dict[str,str]]}
    print(f"Would have returned - {vacation_list}")

    return vacation_list
