# app/config.py
import os
from tortoise import Tortoise


async def init() -> None:
    # Here we connect to a SQLite DB file.
    # also specify the app name of "models"
    # which contain models from "app.models"
    await Tortoise.init(db_url=os.environ.get("DATABASE_URL"), modules={"models": ["app.models"]})
    # Generate the schema
    await Tortoise.generate_schemas()
