from sqlalchemy import Column, Integer, String, Boolean, Float, and_, func, asc, desc
from sqlalchemy.sql.expression import null
from typing import Any, List, Dict, Optional, Union, TypedDict
from sqlalchemy.orm import Session
from app.models.connection import Base, get_db, metadata, engine
from geoalchemy2 import Geometry  # type: ignore
from geoalchemy2.types import Geometry as GeometryType  # type: ignore

"""
CREATE TABLE tripad_final(location_id int,  name text, description text, web_url text, latitude float,
       longitude float, rating float, num_reviews int, address text, city text, one_star int,
       two_star int, three_star int, four_star int, five_star int, rank int, cat text,
       sub_cat text, neighborhood text)
;"""

Coordinate = tuple[float, float]
CoordinateList = list[Coordinate]


class Attractions(Base):  # type: ignore
    __tablename__ = "tripad_final"
    index = Column(Integer, primary_key=True, nullable=False)
    location_id = Column(String, nullable=False)  # Done
    rank = Column(Integer, nullable=False)  # Done
    one_star = Column(Integer, nullable=False)  # Done
    two_star = Column(Integer, nullable=False)  # Done
    three_star = Column(Integer, nullable=False)  # Done
    four_star = Column(Integer, nullable=False)  # Done
    five_star = Column(Integer, nullable=False)  # Done
    latitude = Column(Float, nullable=False)  # Done
    longitude = Column(Float, nullable=False)  # Done
    rating = Column(Float, nullable=False)  # Done
    num_reviews = Column(Integer, nullable=False)  # Done
    name = Column(String, nullable=False)  # Done
    description = Column(String, nullable=False)  # Done
    web_url = Column(String, nullable=False)  # Done
    cat = Column(String, nullable=False)  # Done
    sub_cat = Column(String, nullable=False)  # Done
    neighborhood = Column(String, nullable=False)  # Done
    address = Column(String, nullable=False)  # Done
    city = Column(String, nullable=False)  # Done
    geom = Column(Geometry("POINT"), nullable=False)


AttractionType = Attractions
AttractionsType = List[AttractionType]
ExactAttractionDict = TypedDict(
    "ExactAttractionDict",
    {
        "index": int,
        "location_id": str,
        "name": str,
        "description": Optional[str],
        "web_url": str,
        "latitude": float,
        "longitude": float,
        "rating": float,
        "num_reviews": int,
        "address": str,
        "city": str,
        "one_star": int,
        "two_star": int,
        "three_star": int,
        "four_star": int,
        "five_star": int,
        "rank": int,
        "cat": str,
        "sub_cat": str,
        "neighborhood": str,
        "distance": Optional[float],
    },
)
ExactAttractionsDict = List[ExactAttractionDict]
AttractionDict = Dict[str, Union[str, float, int]]
AttractionsDict = List[AttractionDict]


class AttractionsModel(object):
    def __init__(self) -> None:
        self.__db = next(get_db())
        metadata.create_all()

    def get_all(self) -> AttractionsType:
        return self.get_all_advanced()

    def get_all_advanced(  # noqa: C901
        self,
        rank: Union[int, None] = None,
        one_star: Union[int, None] = None,
        two_star: Union[int, None] = None,
        three_star: Union[int, None] = None,
        four_star: Union[int, None] = None,
        five_star: Union[int, None] = None,
        rating: Union[float, None] = None,
        min_rating: Union[float, None] = None,
        max_rating: Union[float, None] = None,
        min_num_reviews: Union[int, None] = None,
        max_num_reviews: Union[int, None] = None,
        location_id: Union[str, None] = None,
        name_contains: Union[str, None] = None,
        description_contains: Union[str, None] = None,
        cat: Union[str, None] = None,
        sub_cat: Union[str, None] = None,
        web_url: Union[str, None] = None,
        neighborhood: Union[str, None] = None,
        city: Union[str, None] = None,
        coordinate: Union[Coordinate, None] = None,
        limit: Union[int, None] = None,
    ) -> AttractionsType:
        """This is an advanced method for querying the DB, takes a number of arguments for the where clause and applies all of them,
           Any args left as None will not be used.  Returns in raw DB format, will need converted to Dict/JSON for front end

        Args:
            id (Union[str, None], optional): The ID of the record, should only return 1 if this is pased. Defaults to None.
            rank (Union[int, None], optional): The rank you're looking for. Defaults to None.
            one_star (Union[int, None], optional): If you're looking for a one star attraction. Defaults to None.
            two_star (Union[int, None], optional): If you're looking for a two star attraction. Defaults to None.
            three_star (Union[int, None], optional): If you're looking for a three star attraction. Defaults to None.
            four_star (Union[int, None], optional): If you're looking for a four star attraction. Defaults to None.
            five_star (Union[int, None], optional): If you're looking for a five star attraction. Defaults to None.
            rating (Union[float, None], optional): If you're looking for an exact rating. Defaults to None.
            min_rating (Union[float, None], optional): The minimum rating you'll accept. Defaults to None.
            max_rating (Union[float, None], optional): The maximum rating you'll accept. Defaults to None.
            min_num_reviews (Union[int, None], optional): The minimum # of reviews you'll accept. Defaults to None.
            max_num_reviews (Union[int, None], optional): The maximum # of reviews you'll accept. Defaults to None.
            location_id (Union[str, None], optional): The location id to match. Defaults to None.
            name_contains (Union[str, None], optional): To do a contains query on name. Defaults to None.
            description_contains (Union[str, None], optional): The do a contains query on description. Defaults to None.
            cat (Union[str, None], optional): The category you're looking for. Defaults to None.
            sub_cat (Union[str, None], optional): The sub-category you're looking for. Defaults to None.
            web_url (Union[str, None], optional): The weburl of the location. Defaults to None.
            neighborhood (Union[str, None], optional): The neighborhood you're looking for. Defaults to None.
            city (Union[str, None], optional): The city you're looking for. Defaults to None.
            coordinate (Union[Coordinate, None], optional): This lat, lon, will be transposed to db for you (DB takes lon, lat). Defaults to None.
            limit (Union[int, None], optional): The # of records to return. Defaults to None.

        Returns:
            AttractionsType: These are the attractions that met all the filters given
        """
        point = None
        if coordinate is not None:
            print(f"making point from coordinate {coordinate}")
            point = f"POINT({coordinate[1]} {coordinate[0]})"
        res = (
            self.__db.query(Attractions)
            if coordinate is None
            else self.__db.query(
                Attractions,
                func.st_distance(func.ST_GeomFromText(f"{point}", 4326), Attractions.geom).label("distance"),
            )
        )
        if one_star is not None:
            res = res.filter(Attractions.one_star == one_star)
        if two_star is not None:
            res = res.filter(Attractions.two_star == two_star)
        if three_star is not None:
            res = res.filter(Attractions.three_star == three_star)
        if four_star is not None:
            res = res.filter(Attractions.four_star == four_star)
        if five_star is not None:
            res = res.filter(Attractions.five_star == five_star)
        if rank is not None:
            res = res.filter(Attractions.rank == rank)
        if rating is not None:
            res = res.filter(Attractions.rating == rating)
        if min_rating is not None:
            res = res.filter(Attractions.rating >= min_rating)
        if max_rating is not None:
            res = res.filter(Attractions.rating <= max_rating)
        if min_num_reviews is not None:
            res = res.filter(Attractions.num_reviews >= min_num_reviews)
        if max_num_reviews is not None:
            res = res.filter(Attractions.num_reviews <= max_num_reviews)
        if location_id is not None:
            res = res.filter(Attractions.location_id == location_id)
        if name_contains is not None:
            res = res.filter(Attractions.name.ilike(f"%{name_contains}"))
        if description_contains is not None:
            res = res.filter(Attractions.description.ilike(f"%{description_contains}"))
        if cat is not None:
            res = res.filter(Attractions.cat.ilike(f"%{cat}%"))
        if sub_cat is not None:
            res = res.filter(Attractions.sub_cat.ilike(f"%{sub_cat}%"))
        if web_url is not None:
            res = res.filter(Attractions.web_url == web_url)
        if neighborhood is not None:
            res = res.filter(Attractions.neighborhood == neighborhood)
        if city is not None:
            res = res.filter(Attractions.city == city)
        if coordinate is not None:
            # print(f"calling the geo with: {point} <- lon, lat format =(")
            res = res.filter(func.ST_Dwithin(func.ST_GeomFromText(f"{point}", 4326), Attractions.geom, 4000))
            res = res.order_by(asc("distance"))
        if coordinate is None:
            res = res.order_by(desc("rating"))
        if limit is not None:
            res = res.limit(limit)

        # print(f"The statement was - \n {res.statement}")
        return res.all()

    def add(self, data: AttractionDict) -> bool:
        entity = Attractions(**data)
        self.__db.add(entity)
        return entity

    def update(self, location_id: str, data: AttractionDict) -> bool:
        entry = self.__db.query(Attractions).filter(Attractions.location_id == location_id)
        if not entry:
            return False
        else:
            entry.update(data, synchronize_session=False)
            self.__db.commit()
            return True

    def delete(self, location_id: str) -> bool:
        entry = self.__db.query(Attractions).filter(Attractions.location_id == location_id)
        if not entry:
            return False
        else:
            entry.delete()
            self.__db.commit()
            return True
