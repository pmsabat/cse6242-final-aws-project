from sqlalchemy import Column, Integer, String, Boolean, Float, and_, func, desc
from sqlalchemy.sql.expression import null
from typing import Any, List, Dict, Optional, Union, TypedDict
from sqlalchemy.orm import Session
from app.models.connection import Base, get_db, metadata
from geoalchemy2 import Geometry  # type: ignore
from geoalchemy2.types import Geometry as GeometryType  # type: ignore

Coordinate = tuple[float, float]
CoordinateList = list[Coordinate]


# Database schema
class Airbnbs(Base):  # type: ignore
    __tablename__ = "airbnb_final"

    id = Column(Integer, primary_key=True, nullable=False)
    listing_url = Column(String, nullable=False)
    name = Column(String, nullable=False)
    description = Column(String, nullable=False)
    neighborhood_overview = Column(String, nullable=False)
    picture_url = Column(String, nullable=False)
    host_location = Column(String, nullable=False)
    host_about = Column(String, nullable=False)
    host_response_rate = Column(Float, nullable=False)
    host_acceptance_rate = Column(Float, nullable=False)
    host_is_superhost = Column(String, nullable=False)
    host_picture_url = Column(String, nullable=False)
    host_neighbourhood = Column(String, nullable=False)
    host_listings_count = Column(Float, nullable=False)
    host_total_listings_count = Column(Float, nullable=False)
    host_verifications = Column(String, nullable=False)
    host_has_profile_pic = Column(String, nullable=False)
    host_identity_verified = Column(String, nullable=False)
    neighbourhood_cleansed = Column(String, nullable=False)
    latitude = Column(Float, nullable=False)
    longitude = Column(Float, nullable=False)
    property_type = Column(String, nullable=False)
    accommodates = Column(Integer, nullable=False)
    bathrooms_text = Column(String, nullable=False)
    bedrooms = Column(Float, nullable=False)
    beds = Column(Float, nullable=False)
    amenities = Column(String, nullable=False)
    price = Column(Float, nullable=False)
    minimum_nights = Column(Integer, nullable=False)
    maximum_nights = Column(Integer, nullable=False)
    has_availability = Column(String, nullable=False)
    availability_365 = Column(Integer, nullable=False)
    number_of_reviews = Column(Integer, nullable=False)
    review_scores_rating = Column(Float, nullable=False)
    review_scores_accuracy = Column(Float, nullable=False)
    review_scores_cleanliness = Column(Float, nullable=False)
    review_scores_checkin = Column(Float, nullable=False)
    review_scores_communication = Column(Float, nullable=False)
    review_scores_location = Column(Float, nullable=False)
    review_scores_value = Column(Float, nullable=False)
    instant_bookable = Column(String, nullable=False)
    calculated_host_listings_count = Column(Integer, nullable=False)
    calculated_host_listings_count_entire_homes = Column(Integer, nullable=False)
    calculated_host_listings_count_private_rooms = Column(Integer, nullable=False)
    calculated_host_listings_count_shared_rooms = Column(Integer, nullable=False)
    reviews_per_month = Column(Float, nullable=False)


AirbnbType = Airbnbs
AirbnbsType = List[AirbnbType]
ExactAirbnbDict = TypedDict(
    "ExactAirbnbDict",
    {
        "id": int,
        "listing_url": str,
        "name": str,
        "description": str,
        "neighborhood_overview": str,
        "picture_url": str,
        "host_location": str,
        "host_about": str,
        "host_response_rate": float,
        "host_acceptance_rate": float,
        "host_is_superhost": str,
        "host_picture_url": str,
        "host_neighbourhood": str,
        "host_listings_count": float,
        "host_total_listings_count": float,
        "host_verifications": str,
        "host_has_profile_pic": str,
        "host_identity_verified": str,
        "neighbourhood_cleansed": str,
        "latitude": float,
        "longitude": float,
        "property_type": str,
        "accommodates": int,
        "bathrooms_text": str,
        "bedrooms": float,
        "beds": float,
        "amenities": str,
        "price": float,
        "minimum_nights": int,
        "maximum_nights": int,
        "has_availability": str,
        "availability_365": int,
        "number_of_reviews": int,
        "review_scores_rating": float,
        "review_scores_accuracy": float,
        "review_scores_cleanliness": float,
        "review_scores_checkin": float,
        "review_scores_communication": float,
        "review_scores_location": float,
        "review_scores_value": float,
        "instant_bookable": str,
        "calculated_host_listings_count": int,
        "calculated_host_listings_count_entire_homes": int,
        "calculated_host_listings_count_private_rooms": int,
        "calculated_host_listings_count_shared_rooms": int,
        "reviews_per_month": float,
    },
)

ExactAirbnbsDict = List[ExactAirbnbDict]
AirbnbDict = Dict[str, Union[str, float, int]]
AirbnbsDict = List[AirbnbDict]


class AirbnbsModel(object):
    def __init__(self) -> None:
        self.__db = next(get_db())
        metadata.create_all()

    # def get_all(self) -> AirbnbsType:
    #     return self.get_airbnbs()

    def get_airbnbs(  # noqa: C901
        self,
        id: Union[int, None] = None,
        listing_url: Union[str, None] = None,
        name: Union[str, None] = None,
        description: Union[str, None] = None,
        neighborhood_overview: Union[str, None] = None,
        picture_url: Union[str, None] = None,
        host_location: Union[str, None] = None,
        host_about: Union[str, None] = None,
        host_response_rate: Union[float, None] = None,
        host_acceptance_rate: Union[float, None] = None,
        host_is_superhost: Union[str, None] = None,
        host_picture_url: Union[str, None] = None,
        host_neighbourhood: Union[str, None] = None,
        host_listings_count: Union[float, None] = None,
        host_total_listings_count: Union[float, None] = None,
        host_verifications: Union[str, None] = None,
        host_has_profile_pic: Union[str, None] = None,
        host_identity_verified: Union[str, None] = None,
        neighbourhood_cleansed: Union[str, None] = None,
        latitude: Union[float, None] = None,
        longitude: Union[float, None] = None,
        property_type: Union[str, None] = None,
        accommodates: Union[int, None] = None,
        bathrooms_text: Union[str, None] = None,
        bedrooms: Union[float, None] = None,
        beds: Union[float, None] = None,
        amenities: Union[str, None] = None,
        price: Union[float, None] = None,
        minimum_nights: Union[int, None] = None,
        maximum_nights: Union[int, None] = None,
        has_availability: Union[str, None] = None,
        availability_365: Union[int, None] = None,
        number_of_reviews: Union[int, None] = None,
        review_scores_rating: Union[float, None] = None,
        review_scores_accuracy: Union[float, None] = None,
        review_scores_cleanliness: Union[float, None] = None,
        review_scores_checkin: Union[float, None] = None,
        review_scores_communication: Union[float, None] = None,
        review_scores_location: Union[float, None] = None,
        review_scores_value: Union[float, None] = None,
        instant_bookable: Union[str, None] = None,
        calculated_host_listings_count: Union[int, None] = None,
        calculated_host_listings_count_entire_homes: Union[int, None] = None,
        calculated_host_listings_count_private_rooms: Union[int, None] = None,
        calculated_host_listings_count_shared_rooms: Union[int, None] = None,
        reviews_per_month: Union[float, None] = None,
        airbnb_min_cost: Union[float, None] = None,
        airbnb_max_cost: Union[float, None] = None,
    ) -> AirbnbsType:
        """
        Description of function
        """
        res = self.__db.query(Airbnbs)
        if id is not None:
            res = res.filter(Airbnbs.id == id)
        if listing_url is not None:
            res = res.filter(Airbnbs.listing_url.contains(listing_url))  # type: ignore
        if name is not None:
            res = res.filter(Airbnbs.name.contains(name))  # type: ignore
        if description is not None:
            res = res.filter(Airbnbs.description.contains(description))  # type: ignore
        if neighborhood_overview is not None:
            res = res.filter(Airbnbs.neighborhood_overview.contains(neighborhood_overview))  # type: ignore
        if picture_url is not None:
            res = res.filter(Airbnbs.picture_url.contains(picture_url))  # type: ignore
        if host_location is not None:
            res = res.filter(Airbnbs.host_location.contains(host_location))  # type: ignore
        if host_about is not None:
            res = res.filter(Airbnbs.host_location.contains(host_about))  # type: ignore
        if host_response_rate is not None:
            res = res.filter(Airbnbs.host_response_rate >= host_response_rate)
        if host_acceptance_rate is not None:
            res = res.filter(Airbnbs.host_acceptance_rate >= host_acceptance_rate)
        if host_is_superhost is not None:
            res = res.filter(Airbnbs.host_is_superhost.contains(host_is_superhost))  # type: ignore
        if host_picture_url is not None:
            res = res.filter(Airbnbs.host_picture_url.contains(host_picture_url))  # type: ignore
        if host_neighbourhood is not None:
            res = res.filter(Airbnbs.host_neighbourhood.contains(host_neighbourhood))  # type: ignore
        if host_verifications is not None:
            res = res.filter(Airbnbs.host_verifications.contains(host_verifications))  # type: ignore
        if host_has_profile_pic is not None:
            res = res.filter(Airbnbs.host_has_profile_pic.contains(host_has_profile_pic))  # type: ignore
        if host_identity_verified is not None:
            res = res.filter(Airbnbs.host_identity_verified.contains(host_identity_verified))  # type: ignore
        if neighbourhood_cleansed is not None:
            res = res.filter(Airbnbs.neighbourhood_cleansed.contains(neighbourhood_cleansed))  # type: ignore
        if property_type is not None:
            res = res.filter(Airbnbs.property_type.contains(property_type))  # type: ignore
        if bathrooms_text is not None:
            res = res.filter(Airbnbs.bathrooms_text.contains(bathrooms_text))  # type: ignore
        if amenities is not None:
            res = res.filter(Airbnbs.amenities.contains(amenities))  # type: ignore
        if has_availability is not None:
            res = res.filter(Airbnbs.has_availability.contains(has_availability))  # type: ignore
        if instant_bookable is not None:
            res = res.filter(Airbnbs.instant_bookable.contains(instant_bookable))  # type: ignore
        if host_listings_count is not None:
            res = res.filter(Airbnbs.host_listings_count >= host_listings_count)
        if host_total_listings_count is not None:
            res = res.filter(Airbnbs.host_total_listings_count >= host_total_listings_count)
        if latitude is not None:
            res = res.filter(Airbnbs.latitude == latitude)
        if longitude is not None:
            res = res.filter(Airbnbs.longitude == longitude)
        if accommodates is not None:
            res = res.filter(Airbnbs.accommodates >= accommodates)
        if bedrooms is not None:
            res = res.filter(Airbnbs.bedrooms >= bedrooms)
        if beds is not None:
            res = res.filter(Airbnbs.beds >= beds)
        if price is not None:
            res = res.filter(Airbnbs.price.between(airbnb_min_cost, airbnb_max_cost))
        if minimum_nights is not None:
            res = res.filter(Airbnbs.minimum_nights <= minimum_nights)
        if maximum_nights is not None:
            res = res.filter(Airbnbs.maximum_nights >= maximum_nights)
        if availability_365 is not None:
            res = res.filter(Airbnbs.availability_365 >= availability_365)
        if number_of_reviews is not None:
            res = res.filter(Airbnbs.number_of_reviews >= number_of_reviews)
        if review_scores_rating is not None:
            res = res.filter(Airbnbs.review_scores_rating >= review_scores_rating)
        if review_scores_accuracy is not None:
            res = res.filter(Airbnbs.review_scores_accuracy >= review_scores_accuracy)
        if review_scores_cleanliness is not None:
            res = res.filter(Airbnbs.review_scores_cleanliness >= review_scores_cleanliness)
        if review_scores_checkin is not None:
            res = res.filter(Airbnbs.review_scores_checkin >= review_scores_checkin)
        if review_scores_communication is not None:
            res = res.filter(Airbnbs.review_scores_communication >= review_scores_communication)
        if review_scores_location is not None:
            res = res.filter(Airbnbs.review_scores_location >= review_scores_location)
        if review_scores_value is not None:
            res = res.filter(Airbnbs.review_scores_value >= review_scores_value)
        if calculated_host_listings_count is not None:
            res = res.filter(Airbnbs.calculated_host_listings_count >= calculated_host_listings_count)
        if calculated_host_listings_count_entire_homes is not None:
            res = res.filter(
                Airbnbs.calculated_host_listings_count_entire_homes >= calculated_host_listings_count_entire_homes
            )
        if calculated_host_listings_count_private_rooms is not None:
            res = res.filter(
                Airbnbs.calculated_host_listings_count_private_rooms >= calculated_host_listings_count_private_rooms
            )
        if calculated_host_listings_count_shared_rooms is not None:
            res = res.filter(
                Airbnbs.calculated_host_listings_count_shared_rooms >= calculated_host_listings_count_shared_rooms
            )
        if reviews_per_month is not None:
            res = res.filter(Airbnbs.reviews_per_month >= reviews_per_month)

        db_response: AirbnbsType = res.all()
        print("Print res: {}".format(len(db_response)))
        return db_response
