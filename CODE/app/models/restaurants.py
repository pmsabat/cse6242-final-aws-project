from sqlalchemy import Column, Integer, String, Boolean, Float, and_, func, desc, asc
from sqlalchemy.sql.expression import null
from typing import Any, List, Dict, Optional, Union, TypedDict
from sqlalchemy.orm import Session
from app.models.connection import Base, get_db, metadata
from geoalchemy2 import Geometry  # type: ignore
from geoalchemy2.types import Geometry as GeometryType  # type: ignore

Coordinate = tuple[float, float]
CoordinateList = list[Coordinate]


# Database schema
class Restaurants(Base):  # type: ignore
    __tablename__ = "tripad_restaurant_final"

    index = Column(Integer, primary_key=True, nullable=False)
    location_id = Column(String, nullable=False)
    name = Column(String, nullable=False)
    description = Column(String, nullable=False)
    web_url = Column(String, nullable=False)
    latitude = Column(Float, nullable=False)
    longitude = Column(Float, nullable=False)
    rating = Column(Float, nullable=False)
    num_reviews = Column(Integer, nullable=False)
    price_level = Column(String, nullable=False)
    address = Column(String, nullable=False)
    city = Column(String, nullable=False)
    rank = Column(Integer, nullable=False)
    cat = Column(String, nullable=False)
    sub_cat = Column(String, nullable=False)
    min_price = Column(Integer, nullable=False)
    max_price = Column(Integer, nullable=False)
    neighborhood = Column(String, nullable=False)
    cuisines = Column(String, nullable=False)
    prices = Column(String, nullable=False)
    one_star = Column(Integer, nullable=False)
    two_star = Column(Integer, nullable=False)
    three_star = Column(Integer, nullable=False)
    four_star = Column(Integer, nullable=False)
    five_star = Column(Integer, nullable=False)
    service_rating = Column(Float, nullable=False)
    food_rating = Column(Float, nullable=False)
    value_rating = Column(Float, nullable=False)
    atmosphere_rating = Column(Float, nullable=False)
    geom = Column(Geometry("POINT"), nullable=False)


RestaurantType = Restaurants
RestaurantsType = List[RestaurantType]
ExactRestaurantDict = TypedDict(
    "ExactRestaurantDict",
    {
        "index": int,
        "location_id": str,
        "name": str,
        "description": Optional[str],
        "web_url": str,
        "latitude": float,
        "longitude": float,
        "rating": float,
        "num_reviews": int,
        "price_level": str,
        "address": str,
        "city": str,
        "rank": int,
        "cat": str,
        "sub_cat": str,
        "neighborhood": str,
        "cuisines": str,
        "prices": str,
        "one_star": int,
        "two_star": int,
        "three_star": int,
        "four_star": int,
        "five_star": int,
        "min_price": int,
        "max_price": int,
        "service_rating": float,
        "food_rating": float,
        "value_rating": float,
        "atmosphere_rating": float,
        "distance": Optional[float],
    },
)

ExactRestaurantsDict = List[ExactRestaurantDict]
RestaurantDict = Dict[str, Union[str, float, int]]
RestaurantsDict = List[RestaurantDict]


class RestaurantsModel(object):
    def __init__(self) -> None:
        self.__db = next(get_db())
        metadata.create_all()

    def create(
        self,
        location_id: str,
        name: str,
        description: Optional[str],
        web_url: str,
        latitude: float,
        longitude: float,
        rating: float,
        num_reviews: int,
        price_level: str,
        address: str,
        city: str,
        rank: int,
        cat: str,
        sub_cat: str,
        neighborhood: str,
        cuisines: str,
        prices: str,
        one_star: int,
        two_star: int,
        three_star: int,
        four_star: int,
        five_star: int,
        min_price: int,
        max_price: int,
        service_rating: float,
        food_rating: float,
        value_rating: float,
        atmosphere_rating: float,
    ) -> RestaurantsType:
        point = f"POINT({longitude} {latitude})"
        restaurant_to_add = Restaurants(
            location_id=location_id,
            name=name,
            description=description,
            web_url=web_url,
            rating=rating,
            num_reviews=num_reviews,
            price_level=price_level,
            address=address,
            city=city,
            rank=rank,
            cat=cat,
            sub_cat=sub_cat,
            neighborhood=neighborhood,
            cuisines=cuisines,
            prices=prices,
            one_star=one_star,
            two_star=two_star,
            three_star=three_star,
            four_star=four_star,
            five_star=five_star,
            min_price=min_price,
            max_price=max_price,
            service_ratin=service_rating,
            food_rating=food_rating,
            value_rating=value_rating,
            atmosphere_rating=atmosphere_rating,
            geom=func.ST_GeomFromText(f"{point}", 4326),
        )
        self.__db.add(restaurant_to_add)
        return restaurant_to_add

    def get_all(self) -> RestaurantsType:
        return self.get_all_advanced()

    def get_all_advanced(  # noqa: C901
        self,
        rank: Union[int, None] = None,
        one_star: Union[int, None] = None,
        two_star: Union[int, None] = None,
        three_star: Union[int, None] = None,
        four_star: Union[int, None] = None,
        five_star: Union[int, None] = None,
        rating: Union[float, None] = None,
        service_rating: Union[float, None] = None,
        food_rating: Union[float, None] = None,
        value_rating: Union[float, None] = None,
        min_price: Union[int, None] = None,
        max_price: Union[int, None] = None,
        atmosphere_rating: Union[float, None] = None,
        min_rating: Union[float, None] = None,
        max_rating: Union[float, None] = None,
        min_num_reviews: Union[int, None] = None,
        max_num_reviews: Union[int, None] = None,
        location_id: Union[str, None] = None,
        name_contains: Union[str, None] = None,
        description_contains: Union[str, None] = None,
        cat: Union[str, None] = None,
        sub_cat: Union[str, None] = None,
        web_url: Union[str, None] = None,
        cuisines: Union[str, None] = None,
        neighborhood: Union[str, None] = None,
        city: Union[str, None] = None,
        coordinate: Union[Coordinate, None] = None,
        limit: Union[int, None] = None,
    ) -> RestaurantsType:
        """
        Advanced method to query DB which takes a number of arguments for the where clause. Any args left as None are not used.
        Returns raw data which will be converted to JSON/dict
        Args:
            index (Union[str, None], optional): The ID of the record, should only return 1 if this is pased. Defaults to None.
            rank (Union[int, None], optional): The rank you're looking for. Defaults to None.
            one_star (Union[int, None], optional): If you're looking for a one star attraction. Defaults to None.
            two_star (Union[int, None], optional): If you're looking for a two star attraction. Defaults to None.
            three_star (Union[int, None], optional): If you're looking for a three star attraction. Defaults to None.
            four_star (Union[int, None], optional): If you're looking for a four star attraction. Defaults to None.
            five_star (Union[int, None], optional): If you're looking for a five star attraction. Defaults to None.
            rating (Union[float, None], optional): If you're looking for an exact rating. Defaults to None.
            min_rating (Union[float, None], optional): The minimum rating you'll accept. Defaults to None.
            max_rating (Union[float, None], optional): The maximum rating you'll accept. Defaults to None.
            min_num_reviews (Union[int, None], optional): The minimum # of reviews you'll accept. Defaults to None.
            max_num_reviews (Union[int, None], optional): The maximum # of reviews you'll accept. Defaults to None.
            min_price (Union[str, None], optional): The minimum price as $$$ you'll accept. Defaults to None.
            max_price (Union[str, None], optional): The maximum price you'll accept. Defaults to None.
            service_rating (Union[float, None], optional): The service rating you'll accept. Defaults to None.
            food_rating (Union[float, None], optional): The food rating you'll accept. Defaults to None.
            value_rating (Union[float, None], optional): The value rating you'll accept. Defaults to None.
            atmosphere_rating (Union[float, None], optional): The atmosphere rating you'll accept. Defaults to None.
            location_id (Union[str, None], optional): The location id to match. Defaults to None.
            name_contains (Union[str, None], optional): To do a contains query on name. Defaults to None.
            description_contains (Union[str, None], optional): The do a contains query on description. Defaults to None.
            cat (Union[str, None], optional): The category you're looking for. Defaults to None.
            sub_cat (Union[str, None], optional): The sub-category you're looking for. Defaults to None.
            web_url (Union[str, None], optional): The weburl of the location. Defaults to None.
            neighborhood (Union[str, None], optional): The neighborhood you're looking for. Defaults to None.
            city (Union[str, None], optional): The city you're looking for. Defaults to None.
            coordinate (Union[Coordinate, None], optional): This lat, lon, will be transposed to db for you (DB takes lon, lat). Defaults to None.
            limit (Union[int, None], optional): The # of records to return. Defaults to None.
        """
        print(f"Min: {min_price} max: {max_price}")
        point = None
        if coordinate is not None:
            print(f"making point from coordinate {coordinate}")
            point = f"POINT({coordinate[1]} {coordinate[0]})"
        res = (
            self.__db.query(Restaurants)
            if coordinate is None
            else self.__db.query(
                Restaurants,
                func.st_distance(func.ST_GeomFromText(f"{point}", 4326), Restaurants.geom).label("distance"),
            )
        )
        if one_star is not None:
            res = res.filter(Restaurants.one_star == one_star)
        if two_star is not None:
            res = res.filter(Restaurants.two_star == two_star)
        if three_star is not None:
            res = res.filter(Restaurants.three_star == three_star)
        if four_star is not None:
            res = res.filter(Restaurants.four_star == four_star)
        if five_star is not None:
            res = res.filter(Restaurants.five_star == five_star)
        if rank is not None:
            res = res.filter(Restaurants.rank == rank)
        if rating is not None:
            res = res.filter(Restaurants.rating == rating)
        if min_rating is not None:
            res = res.filter(Restaurants.rating >= min_rating)
        if max_rating is not None:
            res = res.filter(Restaurants.rating <= max_rating)
        if service_rating is not None:
            res = res.filter(Restaurants.service_rating == service_rating)
        if food_rating is not None:
            res = res.filter(Restaurants.food_rating == food_rating)
        if value_rating is not None:
            res = res.filter(Restaurants.value_rating == value_rating)
        if atmosphere_rating is not None:
            res = res.filter(Restaurants.atmosphere_rating == atmosphere_rating)
        if min_price is not None:
            res = res.filter(Restaurants.min_price >= min_price)
        if max_price is not None:
            res = res.filter(Restaurants.max_price <= max_price)
            print("Value of max price is \n", max_price)
        if min_num_reviews is not None:
            res = res.filter(Restaurants.num_reviews >= min_num_reviews)
        if max_num_reviews is not None:
            res = res.filter(Restaurants.num_reviews <= max_num_reviews)
        if location_id is not None:
            res = res.filter(Restaurants.location_id == location_id)
        if name_contains is not None:
            res = res.filter(Restaurants.name.ilike(f"%{name_contains}"))
        if description_contains is not None:
            res = res.filter(Restaurants.description.ilike(f"%{description_contains}"))
        if cat is not None:
            res = res.filter(Restaurants.cat.ilike(f"%{cat}%"))
        if sub_cat is not None:
            res = res.filter(Restaurants.sub_cat.ilike(f"%{sub_cat}%"))
        if cuisines is not None:
            res = res.filter(Restaurants.cuisines.ilike(f"%{cuisines}%"))
        if web_url is not None:
            res = res.filter(Restaurants.web_url == web_url)
        if neighborhood is not None:
            res = res.filter(Restaurants.neighborhood == neighborhood)
        if city is not None:
            res = res.filter(Restaurants.city == city)
        if coordinate is not None:
            # print(f"calling the geo with: {point} <- lon, lat format =(")
            res = res.filter(func.ST_Dwithin(func.ST_GeomFromText(f"{point}", 4326), Restaurants.geom, 4000))
            res = res.order_by(asc("distance"))
        if coordinate is None:
            res = res.order_by(desc("rating"))
        if limit is not None:
            res = res.limit(limit)

        # print(f"the query for restaurant is: {res}")
        return res.all()

    def add(self, data: RestaurantDict) -> bool:
        entity = Restaurants(**data)
        self.__db.add(entity)
        return entity

    def update(self, location_id: str, data: RestaurantDict) -> bool:
        entry = self.__db.query(Restaurants).filter(Restaurants.location_id == location_id)
        if not entry:
            return False
        else:
            entry.update(data, synchronize_session=False)
            self.__db.commit()
            return True

    def delete(self, location_id: str) -> bool:
        entry = self.__db.query(Restaurants).filter(Restaurants.location_id == location_id)
        if not entry:
            return False
        else:
            entry.delete()
            self.__db.commit()
            return True
