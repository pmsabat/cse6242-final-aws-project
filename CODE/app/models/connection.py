import asyncio
from typing import Coroutine, Generator, AsyncGenerator
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import Session
from sqlalchemy import MetaData

# from sqlalchemy.ext.asyncio import create_async_engine, async_scoped_session, AsyncSession
from os import getenv

SQL_URL_ENV = f"postgresql://{getenv('POSTGRES_USER')}:{getenv('POSTGRES_PASSWORD')}@{getenv('POSTGRES_URL')}:5432/{getenv('POSTGRES_DB')}"
# SQL_URL = "postgresql://cse6242_user:cse6242_pw@db:5432/cse6242_data"
# print(f"Got SQL URL from ENV as - {SQL_URL_ENV} expected {SQL_URL}")
engine = create_engine(SQL_URL_ENV)
metadata = MetaData(engine)
# async_engine = create_async_engine(SQL_URL_ENV)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
# SessionAsyncFactory = sessionmaker(expire_on_commit=False, class_=AsyncSession, autoflush=False, bind=async_engine)
Base = declarative_base(metadata=metadata)


# Dependency
def get_db() -> Generator[Session, None, None]:
    # print("getting a session ready")
    db = SessionLocal()
    try:
        result = engine.execute("CREATE EXTENSION IF NOT EXISTS postgis;")
        print(f"Tried to create extension: {result}")
        # result2 = engine.execute("""CREATE TABLE IF NOT EXISTS tripad_final(
        #     location_id int,
        #     name text,
        #     description text,
        #     web_url text,
        #     latitude float,
        #     longitude float,
        #     rating float,
        #     num_reviews int,
        #     address text,
        #     city text,
        #     one_star int,
        #     two_star int,
        #     three_star int,
        #     four_star int,
        #     five_star int,
        #     rank int,
        #     cat text,
        #     sub_cat text,
        #     neighborhood text);""")

        # result2 = engine.execute("""CREATE TABLE IF NOT EXISTS tripad_final(
        #     location_id int,
        #     name text,
        #     description text,
        #     web_url text,
        #     latitude float,
        #     longitude float,
        #     rating float,
        #     num_reviews int,
        #     address text,
        #     city text,
        #     one_star int,
        #     two_star int,
        #     three_star int,
        #     four_star int,
        #     five_star int,
        #     rank int,
        #     cat text,
        #     sub_cat text,
        #     neighborhood text);""")

        # result3 = engine.execute("""CREATE TABLE IF NOT EXISTS tripad_restaurant_final(
        #     location_id int,
        #     name text,
        #     description text,
        #     web_url text,
        #     latitude float,
        #     prices text,
        #     longitude float,
        #     rating float,
        #     num_reviews int,
        #     price_level text,
        #     cuisines text,
        #     address text,
        #     city text,
        #     one_star int,
        #     two_star int,
        #     three_star int,
        #     four_star int,
        #     five_star int,
        #     rank int,
        #     cat text,
        #     sub_cat text,
        #     service_rating float, food_rating float, value_rating float, atmosphere_rating float, neighborhood text, min_price int, max_price int);""")
        # cursor.fetchall()
        yield db
    finally:
        db.close()


# async def get_async_db() -> AsyncGenerator[async_scoped_session, None]:
#     db = async_scoped_session(SessionAsyncFactory)
#     try:
#         yield db
#     finally:
#         await db.close()
