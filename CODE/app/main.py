from typing import Any, Dict, List, Type, Union
from fastapi import FastAPI, Request
from pydantic import BaseModel
from app.routers.airbnb import airbnb_router
from app.routers.attractions import attraction_router
from app.routers.restaurant import restaurant_router
from app.routers.vacation import vacation_router
from app.models.connection import Base, engine, SessionLocal, get_db
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.cors import CORSMiddleware
from fastapi.templating import Jinja2Templates

templates = Jinja2Templates(directory="./app/HTMLDirectory")
app = FastAPI()

origins = [
    "http://anaylsis.localhost",
    "https://anaylsis.localhost",
    "http://localhost",
    "http://localhost:10001",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
JSON = Union[Dict[str, Any], List[Any], int, str, float, bool, Type[None]]

Base.metadata.create_all(bind=engine)  # type: ignore

"""
This is the root of the project, returns a hello world json... for now

Returns:
    dict[str,Any]: A Hello world in JSON
"""


class HelloType(BaseModel):
    message: str


# TODO: Change this to static file hosting -
#       See this link for instructions: https://fastapi.tiangolo.com/tutorial/static-files/
#       Basic steps:
#       1. Making a folder in this director (./app/<some static folder>)
#       2. Put the front end files in this folder
#       3. telling the app to mount that folder for that directory
#       4. test that you get the files when going to / (probably analysis.localhost/ )
#       5. test other routes are still available
#       6. have a party, you're awesome
# @app.get("/", response_model=HelloType)
# async def root() -> JSON:
#     return {"message": "Hello World"}


@app.get("/")
def home(request: Request):
    # return {"msg": "Home Page blah blah"}
    return templates.TemplateResponse("planner.html", {"request": request})


class Cat(BaseModel):
    cat: str


@app.get("/airbnb/{zipcode}", response_model=Cat)
async def airbnb(zipcode: int) -> dict[str, str]:
    return {"cat": f"{id}"}


@app.get("/cat/{id}", response_model=Cat)
async def cat(id: str) -> dict[str, str]:
    return {"cat": f"{id}"}


class Cats(BaseModel):
    cat: str
    length: int


@app.get("/cats/{id}", response_model=Cats)
async def cats(id: str) -> dict[str, str]:
    return {"cats": f"{id}", "length": f"{len(id)}"}


app.include_router(airbnb_router)
app.include_router(attraction_router)
app.include_router(restaurant_router)
app.include_router(vacation_router)
