from typing import NewType, Optional, TypedDict, Literal, Union
from aiohttp import ClientSession
from app.models.attractions import ExactAttractionDict, ExactAttractionsDict
from app.models.restaurants import ExactRestaurantDict, ExactRestaurantsDict
from app.models.airbnb import ExactAirbnbDict, ExactAirbnbsDict
from os import environ

ACCESS_TOKEN = environ.get("ACCESS_TOKEN")
transport_types = Literal["driving", "cycling", "walking", "driving-traffic"]
site_types = Literal["airbnb", "attraction", "restaurant"]
Coordinate = tuple[float, float]
CoordinateList = list[Coordinate]
Leg = TypedDict(
    "Leg",
    {
        "weight": float,
        "duration": float,
        "distance": float,
        "steps": list[object],
        "summary": str,
    },
)
ValuedTrip = TypedDict(
    "ValuedTrip",
    {
        "geometry": str,
        "weight": float,
        "weight_name": str,
        "duration": float,
        "cost": float,
        "distance": float,
        "legs": list[Leg],
    },
)

trip = TypedDict(
    "trip",
    {"geometry": str, "weight": float, "weight_name": str, "duration": float, "distance": float, "legs": list[Leg]},
)
waypoint = TypedDict("waypoint", {"name": str, "location": Coordinate, "trip_index": int, "waypoint_index": int})
enhanced_waypoint = TypedDict(
    "enhanced_waypoint",
    {
        "name": str,
        "type": site_types,  # This tells you which key will be populated below
        "location": Coordinate,
        "trip_index": int,
        "waypoint_index": int,
        "airbnb": Optional[ExactAirbnbDict],
        "restaurant": Optional[ExactRestaurantDict],
        "attraction": Optional[ExactAttractionDict],
    },
)
optimization_response = TypedDict(
    "optimization_response", {"code": str, "trips": list[trip], "waypoints": list[enhanced_waypoint]}
)

FullVacationPlanned = TypedDict(
    "FullVacationPlanned", {"code": str, "trips": list[ValuedTrip], "waypoints": list[enhanced_waypoint]}
)


class geoController(object):
    def __init__(self, access_token: Optional[str] = None) -> None:
        token = access_token if access_token is not None else ACCESS_TOKEN
        if token is None:
            raise Exception("Must provide MAPBOX API key to use the geocontroller!")
        self.token: str = token
        print(f"starting with token: {self.token}")

    async def get_address_by_lat_lon(self) -> None:
        ## TODO: code this so it grabs all data from the tripadvisor models
        # and returns it as needed,
        # this should check for invalid input
        pass

    async def get_directions(self) -> None:
        ## TODO: code this so it grabs data (a single tripadvisor)
        # from the models and returns it as needed,
        # this should check for invalid input
        # This should probably take some arguments like id, or lat long?
        pass

    def value_trip(self, time_value: int, trip: trip) -> ValuedTrip:
        valued_trip: ValuedTrip = {
            "cost": (trip["duration"] * time_value) / 3600,
            "geometry": trip["geometry"],
            "weight": trip["weight"],
            "weight_name": trip.get("weight_name", ""),
            "duration": trip["duration"] / 60,
            "distance": trip["distance"],
            "legs": trip["legs"],
        }
        print(f"about to return valued trip - {valued_trip}")
        return valued_trip

    async def solve_tsp(
        self,
        airbnb_coordinates: Coordinate,
        sitesList: CoordinateList,
        transport_method: transport_types = "driving",
    ) -> optimization_response:
        """Given a list of up to twelve locations, solves the TSP problem for the
            optimum route.  Based on: https://docs.mapbox.com/api/navigation/optimization-v1/#optimization-v1-api-pricing

        Args:
            airbnb_coordinates (Coordinate): The coordinates of your airbnb
            attractionsList (CoordinateList): A coordinate list of max length 10
            transport_method (transport_types): The way the user will navigate the route

        Raises:
            Exception: Argument Length Exception, requires less than 10 attractions
            Exception: HTTP Status Code Exception

        Returns:
            optimization_response: The response object from the optimization route
        """
        if len(sitesList) > 10:
            raise Exception("Can not process more than 10 restaurants and attractions!")

        coordinate_string = f"{airbnb_coordinates[1]},{airbnb_coordinates[0]}"
        for coor in sitesList:
            x = coor[0]
            y = coor[1]
            coordinate_string = f"{coordinate_string};{y},{x}"
        # print(f"about to call mapbox with: {coordinate_string}")
        url = f"https://api.mapbox.com/optimized-trips/v1/mapbox/{transport_method}/{coordinate_string}?access_token={self.token}&roundtrip=true&source=first&destination=any"
        print(f"calling the url with: {url}")
        try:
            async with ClientSession() as client:
                # print("stood up aiohttp client")
                async with client.get(url) as resp:
                    if int(resp.status) >= 200 and 400 < int(resp.status):
                        print(f"resp.status was: {resp.status} - \n {resp}")
                        raise Exception("Did not correctly get TSP optimization!")
                    resp_json: optimization_response = await resp.json()
                    print(f"the response was: {resp_json}")
                    return resp_json
        except Exception as ex:
            print(f"Call to mapbox did not work, error - {ex}")
            raise ex
        # https://api.mapbox.com/optimized-trips/v1/mapbox/driving/-122.42,37.78;-122.45,37.91;-122.48,37.73?roundtrip=true&source=first&destination=any&access_token=pk.eyJ1IjoiY292ZXJnaG91bCIsImEiOiJjbGFjenp6MzQwMGh2M25zenJ2am5sb3ZtIn0.Yq-YjDBmngEYp135D9AN1g

    async def weighted_tsp(
        self,
        airbnb: ExactAirbnbDict,
        time_value: int,
        attractionList: ExactAttractionsDict = [],
        restaurantList: ExactRestaurantsDict = [],
        transport_method: transport_types = "driving",
    ) -> FullVacationPlanned:
        """_summary_

        Args:
            airbnb_coordinates (Coordinate): _description_
            attractionsList (CoordinateList): _description_
            time_value (float): _description_
            transport_method (transport_types, optional): _description_. Defaults to "driving".

        Raises:
            Exception: Argument Length Exception, requires less than 10 attractions
            Exception: HTTP Status Code Exception

        Returns:
            valued_trip: The trip the user will travel to meet all routes, weighted with the cost of their time
        """
        siteList: CoordinateList = []
        for attraction in attractionList:
            siteList.append((attraction["latitude"], attraction["longitude"]))

        for restaurant in restaurantList:
            x = float(restaurant["latitude"])
            y = float(restaurant["longitude"])
            siteList.append((x, y))

        airbnb_coordinates = (float(airbnb["latitude"]), float(airbnb["longitude"]))
        optimized_route = await self.solve_tsp(airbnb_coordinates, siteList, transport_method)

        if optimized_route["code"] != "Ok":
            raise Exception(f"Did not correctly solve TSP, but got a 2XX response! {optimized_route.get('message')}")

        trips = optimized_route["trips"]
        print(f"the trips was: {trips}")
        # Update trips with their value to the user
        valued_trips: list[ValuedTrip] = []
        for i in range(len(trips)):
            if "duration" in trips[i]:
                valued_trips.append(self.value_trip(time_value, trips[i]))

        # Update the waypoints with their attractions/airbnb/restaurant details
        enhanced_waypoints: list[enhanced_waypoint] = []

        airbnb_waypoint: enhanced_waypoint = {
            "name": airbnb["name"],
            "type": "airbnb",  # This tells you which key will be populated below
            "location": (airbnb["latitude"], airbnb["longitude"]),
            "trip_index": 0,
            "waypoint_index": 1,
            "airbnb": airbnb,
            "restaurant": None,
            "attraction": None,
        }
        enhanced_waypoints.append(airbnb_waypoint)

        for attraction in attractionList:
            attraction_waypoint: enhanced_waypoint = {
                "name": attraction["name"],
                "type": "attraction",  # This tells you which key will be populated below
                "location": (attraction["latitude"], attraction["longitude"]),
                "trip_index": 0,
                "waypoint_index": 1,
                "airbnb": None,
                "restaurant": None,
                "attraction": attraction,
            }
            enhanced_waypoints.append(attraction_waypoint)

        for restaurant in restaurantList:
            restaurant_waypoint: enhanced_waypoint = {
                "name": restaurant["name"],
                "type": "restaurant",  # This tells you which key will be populated below
                "location": (restaurant["latitude"], restaurant["longitude"]),
                "trip_index": 0,
                "waypoint_index": 1,
                "airbnb": None,
                "restaurant": restaurant,
                "attraction": None,
            }
            enhanced_waypoints.append(restaurant_waypoint)

        evaled_trip: FullVacationPlanned = {
            "code": optimized_route["code"],
            "trips": valued_trips,
            "waypoints": enhanced_waypoints,
        }
        return evaled_trip

    # TODO: Add other methods as needed
