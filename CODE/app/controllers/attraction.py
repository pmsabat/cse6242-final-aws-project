from typing import Optional, Union, Dict, List
from app.models.attractions import (
    AttractionType,
    AttractionsModel,
    AttractionDict,
    AttractionsDict,
    AttractionsType,
    ExactAttractionDict,
    ExactAttractionsDict,
)
from app.controllers.geo import Coordinate
from sqlalchemy.engine.row import Row


class attractionsController(object):
    def __init__(self) -> None:
        self.__model = AttractionsModel()

    def get_all(self) -> ExactAttractionsDict:
        ## TODO: code this so it grabs all data from the tripadvisor models
        # and returns it as needed,
        # this should check for invalid input
        return self.get_all_advanced()

    def update_attractions(self, results):
        # print("M I here")
        for attraction in results:
            # print("Attraction id is :{} and type is {}".format(attraction["location_id"], type(attraction["location_id"])))
            # if the attraction is 'Statue of Liberty' with 103887
            if attraction["location_id"] == 103887 and attraction["name"] == "Statue of Liberty":
                attraction["latitude"] = 40.7029
                attraction["longitude"] = -74.0154
            # if the attraction is 'Staten Island Ferry' with 143363
            if attraction["location_id"] == 143363 and attraction["name"] == "Staten Island Ferry":
                attraction["latitude"] = 40.6647
                attraction["longitude"] = -74.0721
            # if the attraction is 'Ellis Island' with 104370
            if attraction["location_id"] == 104370 and attraction["name"] == "Ellis Island":
                # print("Here...................")
                attraction["latitude"] = 40.7083
                attraction["longitude"] = -74.0349
            # if the attraction is 'Ellis Island Immigration Museum' with 1597190
            if attraction["location_id"] == 1597190 and attraction["name"] == "Ellis Island Immigration Museum":
                attraction["latitude"] = 40.7083
                attraction["longitude"] = -74.0349
            # if the attraction is 'Governors Island National Monument' with 136072
            if attraction["location_id"] == 136072 and attraction["name"] == "Governors Island National Monument":
                attraction["latitude"] = 40.7029
                attraction["longitude"] = -74.0154
            # if the attraction is 'Governors Island' with 12903277
            if attraction["location_id"] == 12903277 and attraction["name"] == "Governors Island":
                attraction["latitude"] = 40.7029
                attraction["longitude"] = -74.0154

        return results

    def get_all_advanced(  # noqa: C901
        self,
        rank: Union[int, None] = None,
        one_star: Union[int, None] = None,
        two_star: Union[int, None] = None,
        three_star: Union[int, None] = None,
        four_star: Union[int, None] = None,
        five_star: Union[int, None] = None,
        rating: Union[float, None] = None,
        min_rating: Union[float, None] = None,
        max_rating: Union[float, None] = None,
        min_num_reviews: Union[int, None] = None,
        max_num_reviews: Union[int, None] = None,
        location_id: Union[str, None] = None,
        name_contains: Union[str, None] = None,
        description_contains: Union[str, None] = None,
        cat: Union[str, None] = None,
        sub_cat: Union[str, None] = None,
        web_url: Union[str, None] = None,
        neighborhood: Union[str, None] = None,
        city: Union[str, None] = None,
        coordinate: Union[Coordinate, None] = None,
        limit: Union[int, None] = None,
    ) -> ExactAttractionsDict:
        ## TODO: code this so it grabs all data from the tripadvisor models
        # and returns it as needed,
        # this should check for invalid input
        # print(f"calling get advanced, coordinate was: {coordinate}")
        result: ExactAttractionsDict = []
        final_result: ExactAttractionsDict = []
        db_result: AttractionsType = self.__model.get_all_advanced(
            rank=rank,
            one_star=one_star,
            two_star=two_star,
            three_star=three_star,
            four_star=four_star,
            five_star=five_star,
            rating=rating,
            min_rating=min_rating,
            max_rating=max_rating,
            min_num_reviews=min_num_reviews,
            max_num_reviews=max_num_reviews,
            location_id=location_id,
            name_contains=name_contains,
            description_contains=description_contains,
            cat=cat,
            sub_cat=sub_cat,
            web_url=web_url,
            neighborhood=neighborhood,
            city=city,
            coordinate=coordinate,
            limit=limit,
        )
        # print(f"The lenght of db result was: {len(db_result)} methods - {dir(db_result)}")
        for data in db_result:
            # print(f"starting with attraction model type: {type(data)}")
            # print(f"starting with coordinate row: {coordinate}")
            distance: Optional[float] = None if not isinstance(data, Row) else data[1]
            extracted_data = data if not isinstance(data, Row) else data[0]
            attraction_dict = extracted_data.__dict__
            # print(f"starting with attraction model: {data}")
            attraction: ExactAttractionDict = {
                "index": attraction_dict["index"],
                "location_id": attraction_dict["location_id"],
                "name": attraction_dict["name"],
                "description": attraction_dict["description"],
                "web_url": attraction_dict["web_url"],
                "latitude": attraction_dict["latitude"],
                "longitude": attraction_dict["longitude"],
                "rating": attraction_dict["rating"],
                "num_reviews": attraction_dict["num_reviews"],
                "address": attraction_dict["address"],
                "city": attraction_dict["city"],
                "one_star": attraction_dict["one_star"],
                "two_star": attraction_dict["two_star"],
                "three_star": attraction_dict["three_star"],
                "four_star": attraction_dict["four_star"],
                "five_star": attraction_dict["five_star"],
                "rank": attraction_dict["rank"],
                "cat": attraction_dict["cat"],
                "sub_cat": attraction_dict["sub_cat"],
                "neighborhood": attraction_dict["neighborhood"],
                "distance": distance,
            }
            # print(f"attraction was: {attraction}")
            result.append(attraction)

        # Update some of the attractions that are in Harbor
        final_result = self.update_attractions(result)
        return final_result

    def get_by_location(self, location_id: str) -> AttractionDict:
        ## TODO: code this so it grabs data (a single tripadvisor)
        # from the models and returns it as needed,
        # this should check for invalid input
        # This should probably take some arguments like id, or lat long?
        results = [data.__dict__ for data in self.__model.get_all_advanced(location_id=location_id)]
        return results[0]

    def add_attraction(self, attraction: AttractionDict) -> bool:
        ## TODO: code this so it adds data (a single tripadvisor)
        # to the database and returns it as needed,
        # this should check for invalid input
        # This should probably take some arguments like id, or lat long?
        return self.__model.add(attraction)

    def delete_attraction(self, location_id: str) -> bool:
        ## TODO: code this so it deletes data (a single tripadvisor)
        # to the database and returns it as needed,
        # this should check for invalid input
        # This should probably take some arguments like id
        return self.__model.delete(location_id)

    def update_attraction(self, location_id: str, attraction: AttractionDict) -> bool:
        ## TODO: code this so it updates data (a single tripadvisor)
        # to the database and returns it as needed,
        # this should check for invalid input
        # This should probably take some arguments like id, and all the fields we're willing to update
        return self.__model.update(location_id, attraction)

    # TODO: Add other methods as needed
