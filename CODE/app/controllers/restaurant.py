from typing import Optional, Union, Dict, List
from app.models.restaurants import (
    RestaurantType,
    RestaurantsModel,
    RestaurantDict,
    RestaurantsDict,
    RestaurantsType,
    ExactRestaurantDict,
    ExactRestaurantsDict,
)
from app.controllers.geo import Coordinate
from sqlalchemy.engine.row import Row


class restaurantsController(object):
    def __init__(self) -> None:
        self.__model = RestaurantsModel()

    def get_all(self) -> ExactRestaurantsDict:
        ## TODO: code this so it grabs all data from the restaurants models
        # and returns it as needed,
        # this should check for invalid input
        return self.get_all_advanced()

    def get_all_advanced(  # noqa: C901
        self,
        rank: Union[int, None] = None,
        one_star: Union[int, None] = None,
        two_star: Union[int, None] = None,
        three_star: Union[int, None] = None,
        four_star: Union[int, None] = None,
        five_star: Union[int, None] = None,
        rating: Union[float, None] = None,
        service_rating: Union[float, None] = None,
        food_rating: Union[float, None] = None,
        value_rating: Union[float, None] = None,
        atmosphere_rating: Union[float, None] = None,
        min_price: Union[int, None] = None,
        max_price: Union[int, None] = None,
        min_rating: Union[float, None] = None,
        max_rating: Union[float, None] = None,
        min_num_reviews: Union[int, None] = None,
        max_num_reviews: Union[int, None] = None,
        location_id: Union[str, None] = None,
        name_contains: Union[str, None] = None,
        description_contains: Union[str, None] = None,
        cat: Union[str, None] = None,
        sub_cat: Union[str, None] = None,
        web_url: Union[str, None] = None,
        neighborhood: Union[str, None] = None,
        city: Union[str, None] = None,
        coordinate: Union[Coordinate, None] = None,
        limit: Union[int, None] = None,
        food_type: Union[str, None] = None,
        food_price: Union[int, None] = None,
    ) -> ExactRestaurantsDict:
        ## TODO: code this so it grabs all data from the tripadvisor models
        # and returns it as needed,
        # this should check for invalid input
        # print(f"calling get advanced, coordinate was: {coordinate}")
        result: ExactRestaurantsDict = []
        db_result: RestaurantsType = self.__model.get_all_advanced(
            rank=rank,
            one_star=one_star,
            two_star=two_star,
            three_star=three_star,
            four_star=four_star,
            five_star=five_star,
            rating=rating,
            service_rating=service_rating,
            food_rating=food_rating,
            value_rating=value_rating,
            atmosphere_rating=atmosphere_rating,
            min_price=min_price,
            max_price=food_price,
            min_rating=min_rating,
            max_rating=max_rating,
            min_num_reviews=min_num_reviews,
            max_num_reviews=max_num_reviews,
            location_id=location_id,
            name_contains=name_contains,
            description_contains=description_contains,
            cat=cat,
            sub_cat=sub_cat,
            cuisines=food_type,
            web_url=web_url,
            neighborhood=neighborhood,
            city=city,
            coordinate=coordinate,
            limit=limit,
        )
        # print(f"The lenght of db result was: {len(db_result)} methods - {dir(db_result)}")
        for data in db_result:
            # print(f"starting with restaurant model type: {type(data)}")
            # print(f"starting with restaurant model row: {data}")
            distance: Optional[float] = None if not isinstance(data, Row) else data[1]
            extracted_data = data if not isinstance(data, Row) else data[0]
            restaurant_dict: Dict[str, Union[str, float, int]] = extracted_data.__dict__
            # print(f"starting with attraction model: {data}")
            restaurant: ExactRestaurantDict = {
                "index": int(restaurant_dict["index"]),
                "location_id": str(restaurant_dict["location_id"]),
                "name": str(restaurant_dict["name"]),
                "description": str(restaurant_dict.get("description")) if restaurant_dict.get("description") else None,
                "web_url": str(restaurant_dict["web_url"]),
                "latitude": float(restaurant_dict["latitude"]),
                "longitude": float(restaurant_dict["longitude"]),
                "rating": float(restaurant_dict["rating"]),
                "num_reviews": int(restaurant_dict["num_reviews"]),
                "min_price": int(restaurant_dict.get("min_price", 0)) if restaurant_dict.get("min_price") else 0,
                "max_price": int(restaurant_dict.get("max_price", 0)) if restaurant_dict.get("max_price") else 0,
                "service_rating": float(restaurant_dict["service_rating"]),
                "food_rating": float(restaurant_dict["food_rating"]),
                "value_rating": float(restaurant_dict["value_rating"]),
                "atmosphere_rating": float(restaurant_dict["atmosphere_rating"]),
                "address": str(restaurant_dict["address"]),
                "city": str(restaurant_dict["city"]),
                "one_star": int(restaurant_dict["one_star"]),
                "two_star": int(restaurant_dict["two_star"]),
                "three_star": int(restaurant_dict["three_star"]),
                "four_star": int(restaurant_dict["four_star"]),
                "five_star": int(restaurant_dict["five_star"]),
                "rank": int(restaurant_dict["rank"]),
                "cat": str(restaurant_dict["cat"]),
                "sub_cat": str(restaurant_dict["sub_cat"]),
                "neighborhood": str(restaurant_dict["neighborhood"]),
                "price_level": str(restaurant_dict["price_level"]),
                "cuisines": str(restaurant_dict["cuisines"]),
                "prices": str(restaurant_dict["prices"]),
                "distance": float(distance) if distance else None,
            }
            # print(f"Rest was: {restaurant}")
            result.append(restaurant)

        return result

    def get_by_location(self, location_id: str) -> RestaurantDict:
        ## TODO: code this so it grabs data (a single tripadvisor)
        # from the models and returns it as needed,
        # this should check for invalid input
        # This should probably take some arguments like id, or lat long?
        results = [data.__dict__ for data in self.__model.get_all_advanced(location_id=location_id)]
        return results[0]

    def add_attraction(self, restaurant: RestaurantDict) -> bool:
        ## TODO: code this so it adds data (a single tripadvisor)
        # to the database and returns it as needed,
        # this should check for invalid input
        # This should probably take some arguments like id, or lat long?
        return self.__model.add(restaurant)

    def delete_attraction(self, location_id: str) -> bool:
        ## TODO: code this so it deletes data (a single tripadvisor)
        # to the database and returns it as needed,
        # this should check for invalid input
        # This should probably take some arguments like id
        return self.__model.delete(location_id)

    def update_attraction(self, location_id: str, restaurant: RestaurantDict) -> bool:
        ## TODO: code this so it updates data (a single tripadvisor)
        # to the database and returns it as needed,
        # this should check for invalid input
        # This should probably take some arguments like id, and all the fields we're willing to update
        return self.__model.update(location_id, restaurant)
