# from app.models.airbnb import Airbnbs
from app.models.airbnb import (
    AirbnbType,
    AirbnbsModel,
    AirbnbDict,
    AirbnbsDict,
    AirbnbsType,
    ExactAirbnbDict,
    ExactAirbnbsDict,
)
from app.controllers.geo import Coordinate
from sqlalchemy.engine.row import Row
from fastapi import Depends
from typing import Any, List, Dict, Optional, Union
from app.models.connection import Base, get_db
import pandas as pd  # type: ignore
from sqlalchemy import create_engine
import numpy.typing as npt  # type: ignore
import numpy as np
from sklearn.cluster import KMeans  # type: ignore
import matplotlib.pyplot as plt  # type: ignore
from sklearn.metrics import classification_report, silhouette_samples, silhouette_score  # type: ignore
import time


class airBnbsController(object):
    def __init__(self) -> None:
        self.__model = AirbnbsModel()

    def _knn(self, airbnbs: list[dict[str, str]]) -> None:
        pass

    # def get_all(self) -> ExactAirbnbsDict:
    #     ## TODO: code this so it grabs all data from the restaurants models
    #     # and returns it as needed,
    #     # this should check for invalid input
    #     return self.get_airbnbs()

    def null_Check(self, df_query: pd.DataFrame) -> pd.DataFrame:
        res = bool(df_query.isnull().values.any())
        if res is True:
            df_query = df_query.dropna()
        return df_query

    def filter_Price_Is_Zero(self, df_query: pd.DataFrame) -> pd.DataFrame:
        df_query = df_query[df_query["price"] != 0]
        return df_query

    def filter_by_user_input(
        self,
        df_query: pd.DataFrame,
        airbnb_min_cost: Union[int, None] = None,
        airbnb_max_cost: Union[int, None] = None,
        accommodation_type: Union[str, None] = None,
        min_nights: Union[int, None] = None,
        max_nights: Union[int, None] = None,
    ) -> pd.DataFrame:
        df_filt = df_query[
            (df_query["price"] >= airbnb_min_cost)
            & (df_query["price"] <= airbnb_max_cost)
            & (df_query["property_type"].str.contains(accommodation_type, na=False))
            & (df_query["minimum_nights"] >= min_nights)
            & (df_query["minimum_nights"] <= max_nights)
        ]
        return df_filt

    def get_Cluster_From_Elbow(self, df_filt: pd.DataFrame) -> tuple[pd.DataFrame, KMeans]:
        try:
            print("length of DF is", len(df_filt))
            print("#####Going to do elbow calculation now#####")
            df_geo = df_filt[["latitude", "longitude"]]
            wcss = []
            # if len(df_filt) > 5:
            for i in range(2, 7):
                kmeans = KMeans(i)
                kmeans.fit(df_geo)
                wcss_iter = kmeans.inertia_
                wcss.append(wcss_iter)
            print("#####Elbow calculation done now####")
            return df_geo, kmeans

        except Exception as e:
            print(f"#####Exception {e} occured while doing elbow calculation#####")
            raise Exception(e)
            # return e

    def silhoute_Score(self, df_filt: pd.DataFrame, df_geo: pd.DataFrame, kmeans: KMeans) -> tuple[dict[str, Any], int]:
        try:
            d = {}
            silhouette_score_average = silhouette_score(df_geo, kmeans.predict(df_geo))
            # print("silhouette_score_average is", silhouette_score_average)

            best_score = -1
            best_k_value = 0
            # let's see the points
            silhouette_score_individual = silhouette_samples(df_geo, kmeans.predict(df_geo))
            # print(silhouette_score_individual)
            # iterate through to find any negative values
            for each_value in silhouette_score_individual:
                if each_value < 0:
                    # print(f"We have found a negative silhouette score: {each_value}")
                    pass

            # re-do our loop, try to find values with no negative scores, or one with the least!!
            bad_k_values = {}
            possible_K_values = [i for i in range(2, 7)]

            # iterate through each of our values
            for each_value in possible_K_values:
                # iterate through, taking each value from
                model = KMeans(n_clusters=each_value, init="k-means++", random_state=32)
                # fit it
                model.fit(df_geo)
                # find each silhouette score
                silhouette_score_individual = silhouette_samples(df_geo, model.predict(df_geo))
                # print(silhouette_score_individual)
                # iterate through to find any negative values
                for each_silhouette in silhouette_score_individual:
                    # if we find a negative, lets start counting them
                    if each_silhouette < 0:
                        if each_value not in bad_k_values:
                            bad_k_values[each_value] = 1
                        else:
                            bad_k_values[each_value] += 1

            # print("Bad k value: {}".format(bad_k_values))
            for key, val in bad_k_values.items():
                # print(f" This Many Clusters: {key} | Number of Negative Values: {val}")
                d[key] = val

            # print("D value: {}".format(d))
            # Ignoring type because d.get is a callable function, seems to be confusion error
            if d:
                best_k_value: int = min(d, key=d.get)  # type: ignore
            else:
                #  model = KMeans(n_clusters=25, init='k-means++',random_state=32)# re-fit our model model.fit(df_scaled)
                for i in range(2, 7):
                    model = KMeans(n_clusters=i, init="k-means++", random_state=32)
                    model.fit(df_geo)
                    silhouette_score_average = silhouette_score(df_geo, model.predict(df_geo))
                    if silhouette_score_average > best_score:
                        best_score = silhouette_score_average
                        best_k_value = i
                # print("Best score : {} and best k value: {}".format(best_score, best_k_value))
                # return d, best_k_value

            # print("Best k value is", best_k_value)
            return d, best_k_value
        except Exception as e:
            print(f"######Exception {e} occured while doing silhoutte score calculation#####")
            raise Exception(e)

    def train_Model(self, df_geo: pd.DataFrame, best_k_value: int) -> tuple[pd.DataFrame, npt.NDArray[np.int32]]:
        try:
            kmeans = KMeans(init="random", n_clusters=best_k_value, n_init=10, max_iter=300, random_state=24)
            # fitted =
            kmeans.fit_predict(df_geo)
            labels = kmeans.labels_
            # df_geo["cluster"] = labels
            return df_geo, labels
        except Exception as e:
            print(f"#####Exception {e} occured while training model for clustering#####")
            raise Exception(e)

    def top_Five(
        self, df_filt: pd.DataFrame, labels: npt.NDArray[np.int32], best_k_value: int
    ) -> tuple[dict[str, Any], dict[str, Any]]:
        try:
            df_filt["cluster"] = labels

            sorted_clusters = {}

            for i in range(len(df_filt.cluster.unique())):
                # sorted_clusters["df_filtered"+str(i)]=pd.DataFrame()
                sorted_clusters["df_filtered" + str(i)] = df_filt[df_filt["cluster"] == i]
                sorted_clusters["df_filtered" + str(i)] = sorted_clusters["df_filtered" + str(i)].sort_values(
                    ["price", "review_scores_rating"], ascending=[True, False]
                )
                sorted_clusters["df_filtered" + str(i)]["id"] = sorted_clusters["df_filtered" + str(i)]["id"].astype(
                    "int"
                )
                # print("rows in df " + str("df_filtered" + str(i)) + " are", len(sorted_clusters["df_filtered" + str(i)]))

            top_5 = {}
            for k, v in sorted_clusters.items():
                top_5[k] = sorted_clusters[k].head(1)
                # print("rows in top_5 df "+str("df_filtered"+str(i))+ " are" ,len(top_5["df_filtered"+str(i)]))

            return sorted_clusters, top_5
        except Exception as e:
            print(f"#####Exception {e} occured while getting top_5 airbnbs#####")
            raise Exception(e)

    def convert_to_dictionary(self, data: Any) -> list[ExactAirbnbDict]:
        try:
            final_list = []
            data = data.to_dict()
            # first get the five ids
            key_id = []
            for id, loc_id in data["id"].items():
                key_id.append(id)

            for each_key in key_id:
                entry = {}
                entry["key_id"] = each_key
                for key, val in data.items():
                    entry[key] = val[each_key]
                final_list.append(entry)

            return final_list  # type: ignore
        except Exception as e:
            print(f"#####Exception {e} occured while converting dataframe to dictionary#####")
            raise Exception(e)

    def call_All(self, df_filt: pd.DataFrame) -> dict[str, Any]:
        df_geo, kmeans = self.get_Cluster_From_Elbow(df_filt)
        d, best_k_value = self.silhoute_Score(df_filt, df_geo, kmeans)
        df_geo, labels = self.train_Model(df_geo, best_k_value)
        # dict_of_dfs = self.visualize_Cluster(df_geo, best_k_value)
        sorted_clusters, top_5 = self.top_Five(df_filt, labels, best_k_value)
        return top_5

    def get_airbnbs(  # noqa: C901
        self,
        listing_url: Union[str, None] = None,
        name: Union[str, None] = None,
        description: Union[str, None] = None,
        neighborhood_overview: Union[str, None] = None,
        picture_url: Union[str, None] = None,
        host_location: Union[str, None] = None,
        host_about: Union[str, None] = None,
        host_response_rate: Union[float, None] = None,
        host_acceptance_rate: Union[float, None] = None,
        host_is_superhost: Union[str, None] = None,
        host_picture_url: Union[str, None] = None,
        host_neighbourhood: Union[str, None] = None,
        host_listings_count: Union[float, None] = None,
        host_total_listings_count: Union[float, None] = None,
        host_verifications: Union[str, None] = None,
        host_has_profile_pic: Union[str, None] = None,
        host_identity_verified: Union[str, None] = None,
        neighbourhood_cleansed: Union[str, None] = None,
        latitude: Union[float, None] = None,
        longitude: Union[float, None] = None,
        property_type: Union[str, None] = None,
        accommodates: Union[int, None] = None,
        bathrooms_text: Union[str, None] = None,
        bedrooms: Union[float, None] = None,
        beds: Union[float, None] = None,
        amenities: Union[str, None] = None,
        price: Union[float, None] = None,
        minimum_nights: Union[int, None] = None,
        maximum_nights: Union[int, None] = None,
        has_availability: Union[str, None] = None,
        availability_365: Union[int, None] = None,
        number_of_reviews: Union[int, None] = None,
        review_scores_rating: Union[float, None] = None,
        review_scores_accuracy: Union[float, None] = None,
        review_scores_cleanliness: Union[float, None] = None,
        review_scores_checkin: Union[float, None] = None,
        review_scores_communication: Union[float, None] = None,
        review_scores_location: Union[float, None] = None,
        review_scores_value: Union[float, None] = None,
        instant_bookable: Union[str, None] = None,
        calculated_host_listings_count: Union[int, None] = None,
        calculated_host_listings_count_entire_homes: Union[int, None] = None,
        calculated_host_listings_count_private_rooms: Union[int, None] = None,
        calculated_host_listings_count_shared_rooms: Union[int, None] = None,
        reviews_per_month: Union[float, None] = None,
        airbnb_min_cost: Union[float, None] = None,
        airbnb_max_cost: Union[float, None] = None,
        accommodation_type: Union[str, None] = None,
        min_nights: Union[int, None] = None,
        max_nights: Union[int, None] = None,
        guests: Union[int, None] = None,
    ) -> ExactAirbnbsDict:
        ## TODO: code this so it grabs all data from the airbnb models
        main_start = time.time()
        # print(f"calling get advanced, coordinate was: {coordinate}")
        result: List[Dict[str, Any]] = []
        # print(">>>>>>>>> I am here in 1st call")
        db_result: AirbnbsType = self.__model.get_airbnbs(
            listing_url=listing_url,
            name=name,
            description=description,
            neighborhood_overview=neighborhood_overview,
            picture_url=picture_url,
            host_location=host_location,
            host_about=host_about,
            host_response_rate=host_response_rate,
            host_acceptance_rate=host_acceptance_rate,
            host_is_superhost=host_is_superhost,
            host_picture_url=host_picture_url,
            host_neighbourhood=host_neighbourhood,
            host_listings_count=host_listings_count,
            host_total_listings_count=host_total_listings_count,
            host_verifications=host_verifications,
            host_has_profile_pic=host_has_profile_pic,
            host_identity_verified=host_identity_verified,
            neighbourhood_cleansed=neighbourhood_cleansed,
            latitude=latitude,
            longitude=longitude,
            property_type=accommodation_type,
            accommodates=guests,
            bathrooms_text=bathrooms_text,
            bedrooms=bedrooms,
            beds=beds,
            amenities=amenities,
            price=airbnb_min_cost,
            minimum_nights=min_nights,
            maximum_nights=maximum_nights,
            has_availability=has_availability,
            availability_365=availability_365,
            number_of_reviews=number_of_reviews,
            review_scores_rating=review_scores_rating,
            review_scores_accuracy=review_scores_accuracy,
            review_scores_cleanliness=review_scores_cleanliness,
            review_scores_checkin=review_scores_checkin,
            review_scores_communication=review_scores_communication,
            review_scores_location=review_scores_location,
            review_scores_value=review_scores_value,
            instant_bookable=instant_bookable,
            calculated_host_listings_count=calculated_host_listings_count,
            calculated_host_listings_count_entire_homes=calculated_host_listings_count_entire_homes,
            calculated_host_listings_count_private_rooms=calculated_host_listings_count_private_rooms,
            calculated_host_listings_count_shared_rooms=calculated_host_listings_count_shared_rooms,
            reviews_per_month=reviews_per_month,
            airbnb_min_cost=airbnb_min_cost,
            airbnb_max_cost=airbnb_max_cost,
        )

        # If not result return empty
        if not db_result:
            return []

        # Process results
        for data in db_result:
            result.append(data.__dict__)

        if len(result) <= 7:
            typed_result: ExactAirbnbsDict = []
            for r in result:
                typed_result.append(
                    ExactAirbnbDict(
                        {
                            "id": int(r.get("id", 0)),
                            "listing_url": str(r.get("listing_url", "")),
                            "name": str(r.get("name", "")),
                            "description": str(r.get("description", "")),
                            "neighborhood_overview": str(r["neighborhood_overview"]),
                            "picture_url": str(r.get("picture_url", "")),
                            "host_location": str(r.get("host_location", "")),
                            "host_about": str(r.get("host_about", "")),
                            "host_response_rate": float(r.get("host_response_rate", 0.0)),
                            "host_acceptance_rate": float(r.get("host_acceptance_rate", 0.0)),
                            "host_is_superhost": str(r.get("host_is_superhost", "")),
                            "host_picture_url": str(r.get("host_picture_url", "")),
                            "host_neighbourhood": str(r.get("host_neighbourhood", "")),
                            "host_listings_count": float(r.get("host_listings_count", 0.0)),
                            "host_total_listings_count": float(r.get("host_total_listings_count", 0.0)),
                            "host_verifications": str(r.get("host_verifications", "")),
                            "host_has_profile_pic": str(r.get("host_has_profile_pic", "")),
                            "host_identity_verified": str(r.get("host_identity_verified", "")),
                            "neighbourhood_cleansed": str(r.get("neighbourhood_cleansed", "")),
                            "latitude": float(r.get("latitude", 0.0)),
                            "longitude": float(r.get("longitude", 0.0)),
                            "property_type": str(r.get("property_type", "")),
                            "accommodates": int(r.get("accommodates", 0)),
                            "bathrooms_text": str(r.get("bathrooms_text", "")),
                            "bedrooms": float(r.get("bedrooms", 0.0)),
                            "beds": float(r.get("beds", 0.0)),
                            "amenities": str(r.get("amenities", "")),
                            "price": float(r.get("price", 0.0)),
                            "minimum_nights": int(r.get("minimum_nights", 0)),
                            "maximum_nights": int(r.get("maximum_nights", 0)),
                            "has_availability": str(r.get("has_availability", "")),
                            "availability_365": int(r.get("availability_365", 0)),
                            "number_of_reviews": int(r.get("number_of_reviews", 0)),
                            "review_scores_rating": float(r.get("review_scores_rating", 0.0)),
                            "review_scores_accuracy": float(r.get("review_scores_accuracy", 0.0)),
                            "review_scores_cleanliness": float(r.get("review_scores_cleanliness", 0.0)),
                            "review_scores_checkin": float(r.get("review_scores_checkin", 0.0)),
                            "review_scores_communication": float(r.get("review_scores_communication", 0.0)),
                            "review_scores_location": float(r.get("review_scores_location", 0.0)),
                            "review_scores_value": float(r.get("review_scores_value", 0.0)),
                            "instant_bookable": str(r.get("instant_bookable", "")),
                            "calculated_host_listings_count": int(r.get("calculated_host_listings_count", 0)),
                            "calculated_host_listings_count_entire_homes": int(
                                r.get("calculated_host_listings_count_entire_homes", 0)
                            ),
                            "calculated_host_listings_count_private_rooms": int(
                                r.get("calculated_host_listings_count_private_rooms", 0)
                            ),
                            "calculated_host_listings_count_shared_rooms": int(
                                r.get("calculated_host_listings_count_shared_rooms", 0)
                            ),
                            "reviews_per_month": float(r.get("reviews_per_month", 0.0)),
                        }
                    )
                )
            return typed_result

        # convert results to dataframe
        df_query = pd.DataFrame(result)
        try:
            df_query = df_query.drop("_sa_instance_state", axis=1)
        except Exception as ex:
            print(f"The query did not have this key! {ex}")

        # Filter data inputs
        df_query = self.null_Check(df_query)
        # print(f"Going into price the dataframe is: {df_query.head(5)}")
        df_query = self.filter_Price_Is_Zero(df_query)
        # df_filt = self.filter_by_user_input(
        #     df_query, airbnb_min_cost, airbnb_max_cost, accommodation_type, min_nights, max_nights
        # )
        df_filt = df_query
        # print("after filteration df_filt df is :\n", df_filt)

        # Call methods to cluster Airbnbs and return the top 5 for each cluster
        top_5 = self.call_All(df_filt)
        # print("top_5 is:\n", top_5)
        # print("type of top_5 is:\n", type(top_5))

        # serialize the top_5 for each cluster as a list
        serialized_data = {}
        for key, val in top_5.items():
            temp_top_5_list = self.convert_to_dictionary(val)
            serialized_data[key] = temp_top_5_list

        # Combine the final result
        final_result = []
        for each_cluster, cluster_data in serialized_data.items():
            final_result.append(cluster_data[0])

        main_end = time.time()
        print("Entire function time : {}".format(main_end - main_start))
        return final_result

    def get_airbnb(self) -> None:
        ## TODO: code this so it grabs data (a single airbnb)
        # from the models and returns it as needed,
        # this should check for invalid input
        # This should probably take some arguments like id, or lat long?
        pass

    def add_airbnb(self) -> None:
        ## TODO: code this so it adds data (a single airbnb)
        # to the database and returns it as needed,
        # this should check for invalid input
        # This should probably take some arguments like id, or lat long?
        pass

    def delete_airbnb(self) -> None:
        ## TODO: code this so it deletes data (a single airbnb)
        # to the database and returns it as needed,
        # this should check for invalid input
        # This should probably take some arguments like id
        pass

    def update_airbnb(self) -> None:
        ## TODO: code this so it updates data (a single airbnb)
        # to the database and returns it as needed,
        # this should check for invalid input
        # This should probably take some arguments like id, and all the fields we're willing to update
        pass

    # TODO: Add other methods as needed
