resource "aws_acm_certificate" "full_domain" {
  domain_name       = "${var.subdomain}.${var.domain}"
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

data "aws_route53_zone" "full_domain" {
  name = "${var.domain}."
}

resource "aws_route53_record" "full_domain_validation" {
  depends_on = [aws_acm_certificate.full_domain]
  name    = element(tolist(aws_acm_certificate.full_domain.domain_validation_options), 0)["resource_record_name"]
  type    = element(tolist(aws_acm_certificate.full_domain.domain_validation_options), 0)["resource_record_type"]
  zone_id = data.aws_route53_zone.full_domain.zone_id
  records = [element(tolist(aws_acm_certificate.full_domain.domain_validation_options), 0)["resource_record_value"]]
  ttl     = 300
}

resource "aws_acm_certificate_validation" "full_domain" {
  certificate_arn         = aws_acm_certificate.full_domain.arn
  validation_record_fqdns = aws_route53_record.full_domain_validation.*.fqdn
}