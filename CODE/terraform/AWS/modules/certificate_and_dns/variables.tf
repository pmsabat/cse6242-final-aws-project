variable "domain" {
  description = "The primary domain this will be hosted on"
  default     = "sabs.me"
  type = string
}

variable "subdomain" {
  description = "The sub-domain this will be hosted on"
  default     = "vacation"
  type = string
}