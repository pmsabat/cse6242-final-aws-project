output "acm_certificate_domain_name" {
  description = "The domain name the acm certificate is issued for"
  value       = try(aws_acm_certificate.full_domain.domain_name, "")
}

output "acm_certificate_arn" {
  description = "The arn of the acm certificate for the domain"
  value       = try(aws_acm_certificate.full_domain.arn, "")
}

output "acm_certificate_id" {
  description = "The id of the acm certificate for the domain"
  value       = try(aws_acm_certificate.full_domain.id, "")
}