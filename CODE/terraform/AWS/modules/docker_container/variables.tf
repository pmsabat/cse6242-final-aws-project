variable "container_name" {
  description = "The name to give the container"
  default     = "vacation-prod"
  type        = string
}

variable "container_tag" {
  description = "The tag to give the container"
  default     = "latest"
  type        = string
}

variable "docker_file_location" {
  description = "The directory to build from"
  default     = "../../"
  type        = string
}

variable "docker_build_platform" {
  description = "The platform to build the docker container for"
  default     = "linux/amd64"
  type        = string
}

variable "docker_source_directory" {
  description = "The source directory for files that are used to build the container"
  default     = "app"
  type        = string
}

variable "registry_address" {
  description = "The url to push with"
  type        = string
}

variable "login_address" {
  description = "The url to authenticate with"
  type        = string
}

variable "registry_password" {
  description = "The password to use for authentication"
  default     = null
  type        = string
}

variable "registry_username" {
  description = "The username to use for authentication"
  type        = string
}

variable "region" {
  description = "The region the ECR registry is located in"
  type = string
}