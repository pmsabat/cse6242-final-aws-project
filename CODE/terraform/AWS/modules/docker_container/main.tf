terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.23.1"
    }
  }

}

provider "docker" {
  # host = "unix:///var/run/docker.sock"
  registry_auth {
      address = "${var.registry_address}"
      username = "${var.registry_username}"
      password = "${var.registry_password}"
  }
}

resource "docker_image" "image" {
  name = var.container_name
  build {
    path = "${path.root}/${var.docker_file_location}"
    tag  = [var.container_tag]
    build_arg = {
      platform = var.docker_build_platform
    }
  }
  triggers = {
    dir_sha1 = sha1(
      join(
        "", 
        [
          for f in fileset("${path.root}/${var.docker_file_location}", "${var.docker_source_directory}/*") : 
            filesha1("${path.root}/${var.docker_file_location}/${f}")
        ]
      )
    )
  }

    provisioner "local-exec" {
    command = <<-EOT
    echo operating with $REGION on user: $USER for repo: $REPOSITORY_URL on image: $IMAGE_NAME on tag: $TAG && \
    aws ecr get-login-password --region $REGION | docker login --username $USER --password-stdin $REPOSITORY_URL && \
    docker tag $IMAGE_NAME:$TAG $REPOSITORY_URL:$TAG && docker push $REPOSITORY_URL:$TAG;
    EOT
    environment = {
      REGION = var.region
      USER = try(var.registry_username, "AWS")
      REPOSITORY_URL = var.registry_address
      IMAGE_NAME = docker_image.image.name
      TAG = var.container_tag
     }
  }
}

# resource "null_resource" "push" {
#   triggers = {
#     dir_sha1 = sha1(
#       join(
#         "", 
#         [
#           for f in fileset("${path.root}/${var.docker_file_location}", "${var.docker_source_directory}/*") : 
#             filesha1("${path.root}/${var.docker_file_location}/${f}")
#         ]
#       )
#     )
#   }


#   depends_on = [
#     docker_image.image
#   ]
# }

# resource "docker_registry_image" "remote_image" {
#   name = "${var.container_name}:${var.container_tag}"

#   build {
#     context = "${path.root}/${var.docker_file_location}"
#     build_args = {
#       platform = var.docker_build_platform
#     }
#   }
#   # depends_on = [
#   #   docker_image.image
#   # ]
# }
