output db_url {
    description = "The DNS resolvable name to connect to the postgres instance"
    value = aws_db_instance.postgres.address
}

output db_user {
    description = "The user that is default created for RDS"
    value = aws_db_instance.postgres.username
}

output db_password {
    description = "The user that is default created for RDS"
    value = aws_db_instance.postgres.password
}

output db_arn {
    value = aws_db_instance.postgres.arn
}

output id {
    value = aws_db_instance.postgres.id
}

output identifier {
    value = aws_db_instance.postgres.identifier
}

output db_timezone {
    value = aws_db_instance.postgres.timezone
}

output db_allocated_storage_gb {
    value = aws_db_instance.postgres.allocated_storage
}

output db_engine {
    value = aws_db_instance.postgres.engine
}

output db_engine_version {
    value = aws_db_instance.postgres.engine_version
}

output db_security_group_id {
    value = aws_security_group.rds.id
}

output db_security_group_arn {
    value = aws_security_group.rds.arn
}