variable "db_user" {
  description = "The user for RDS"
  default     = "cse6242_user"
  type = string
}

variable "identifier" {
  description = "The identifier for the RDS instance"
  type = string
}

variable "db_password" {
  description = "The password for RDS"
  default     = "cse6242_pw"
  type = string
}

variable "db_name" {
  description = "The database to query for in RDS"
  default     = "postgres"
  type = string
}

variable vpc_id {
  description = "The VPC ID for the RDS to exist in"
  default     = []
}

variable public_subnets {
  description = "List of subnets RDS can exist in"
  default     = []
}

variable "primary_db_security_group" {
  description = "Security Group ID to have access to the RDS instance"
  default     = ""
}

variable "additional_db_security_groups" {
  description = "List of Security Group IDs to have access to the RDS instance"
  default     = []
}

variable "rds_instance_size" {
  description = "The size of RDS instance, eg db.t2.micro"
  default     = "db.t3.micro"
  type = string
}

variable "rds_storage_size" {
  description = "The size of RDS instance's storage in GB"
  default     = 30
  type = number
}
