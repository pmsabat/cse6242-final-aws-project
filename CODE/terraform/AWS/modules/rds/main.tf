#######################
# Dependancies, for RDS
#######################
resource "aws_db_subnet_group" "rds" {
  name       = "rds-subnet"
  subnet_ids = var.public_subnets

  tags = {
    Name = "Vacation"
  }
}

resource "aws_db_parameter_group" "rds" {
  name   = "rds-parameters"
  family = "postgres13"

  parameter {
    name  = "log_connections"
    value = "1"
  }
}

# ECS/Proxy to RDS security group rule (aka allow access from ECS to our DB
resource "aws_security_group" "rds" {
  name        = "rds"
  description = "allow inbound access from the fargate tasks and specified other security groups only"
  vpc_id      = var.vpc_id

  ingress {
    protocol        = "tcp"
    from_port       = "5432"
    to_port         = "5432"
    security_groups = concat([var.primary_db_security_group], var.additional_db_security_groups)
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

########################
# RDS - aka our postgres
########################
resource "aws_db_instance" "postgres" {
  identifier             = var.identifier
  instance_class         = var.rds_instance_size
  allocated_storage      = var.rds_storage_size
  engine                 = "postgres"
  engine_version         = "13.7"
  username               = var.db_user
  password               = var.db_password
  db_subnet_group_name   = aws_db_subnet_group.rds.name
  vpc_security_group_ids = [aws_security_group.rds.id]
  parameter_group_name   = aws_db_parameter_group.rds.name
  publicly_accessible    = false
  skip_final_snapshot    = true
  storage_type           = "gp2"
  maintenance_window     = "sun:02:00-sun:04:00"
}