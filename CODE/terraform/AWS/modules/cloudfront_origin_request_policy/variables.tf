variable name {
    description = "The name of the policy"
    type        = string
    default     = "ForwardAll"
}

variable cookie_behavior {
    description = "The behavior for the cookie for requests to origin"
    type        = string
    default     = "all"
}

variable query_string_behavior {
    description = "The behavior for the query string for requests to origin"
    type        = string
    default     = "all"
}

variable header_behavior {
    description = "The behavior for the headers for requests to origin"
    type        = string
    default     = "allViewerAndWhitelistCloudFront"
}

variable header_items {
    description = "The headers to send back to the origin"
    type        = list
    default     = [
        "CloudFront-Viewer-Header-Order",
        "CloudFront-Viewer-Time-Zone",
        "CloudFront-Viewer-Address",
        "CloudFront-Viewer-Country",
        "CloudFront-Is-IOS-Viewer",
        "CloudFront-Viewer-Country-Name",
        "CloudFront-Is-Tablet-Viewer",
        "CloudFront-Forwarded-Proto",
        "CloudFront-Is-Mobile-Viewer",
        "CloudFront-Viewer-Country-Region",
        "CloudFront-Is-SmartTV-Viewer",
        "CloudFront-Is-Android-Viewer",
        "CloudFront-Viewer-Header-Count",
        "CloudFront-Viewer-City",
        "CloudFront-Viewer-Latitude",
        "CloudFront-Viewer-Longitude",
        "CloudFront-Viewer-Http-Version",
        "CloudFront-Viewer-Postal-Code",
        "CloudFront-Viewer-ASN",
        "CloudFront-Viewer-JA3-Fingerprint",
        "CloudFront-Is-Desktop-Viewer",
        "CloudFront-Viewer-Metro-Code",
        "CloudFront-Viewer-TLS",
        "CloudFront-Viewer-Country-Region-Name",
      ]
}

variable cookie_items {
    description = "The cookies to send back to the origin"
    type        = list
    default     = []
}

variable query_strings_items {
    description = "The  query string key/values to send back to the origin"
    type        = list
    default     = []
}