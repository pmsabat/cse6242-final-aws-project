output "id" {
  description = "id of the CF policy"
  value       = aws_cloudfront_origin_request_policy.this.id
}

output "etag" {
  description = "etag of the CF policy"
  value       = aws_cloudfront_origin_request_policy.this.etag
}