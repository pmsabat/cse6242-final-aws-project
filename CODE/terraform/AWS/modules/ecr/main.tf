resource "aws_ecr_repository" "mutable_repo" {
  name                 = "${var.image_name}"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = var.scan_on_push
  }
  force_delete = true
}

resource "aws_ecr_lifecycle_policy" "expires_policy" {
  repository = aws_ecr_repository.mutable_repo.name

  policy = jsonencode({
    rules = [{
      rulePriority = 1
      description  = "keep last 10 images"
      action       = {
        type = "expire"
      }
      selection     = {
        tagStatus   = "any"
        countType   = "imageCountMoreThan"
        countNumber = 10
      }
    }]
  })
}

data "aws_ecr_authorization_token" "token" {
    # registry_id = "${aws_ecr_repository.mutable_repo.id}"
}