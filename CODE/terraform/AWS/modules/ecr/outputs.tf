output "ecr_login_url" {
  description = "Login endpoint to ECR registry"
  value       = try(data.aws_ecr_authorization_token.token.proxy_endpoint, "")
}

output "ecr_username" {
  description = "Login username to ECR registry"
  value       = try(data.aws_ecr_authorization_token.token.user_name, "")
}

output "ecr_password" {
  description = "Login password to ECR registry"
  value       = try(data.aws_ecr_authorization_token.token.password, "")
}

output "ecr_auth_expires" {
  description = "When the username and password for the ECR will expire"
  value       = try(data.aws_ecr_authorization_token.token.expires_at, "")
}

output "ecr_arn" {
  description = "The arn for the ECR"
  value       = try(aws_ecr_repository.mutable_repo.arn, "")
}

output "ecr_url" {
  description = "The arn for the ECR"
  value       = try(aws_ecr_repository.mutable_repo.repository_url, "")
}

output "ecr_id" {
  description = "The id for the ECR"
  value       = try(aws_ecr_repository.mutable_repo.registry_id, "")
}