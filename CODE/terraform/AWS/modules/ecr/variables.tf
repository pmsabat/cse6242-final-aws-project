variable "image_name" {
  description = "The name to give the container"
  default     = "vacation-prod"
  type        = string
}

variable "scan_on_push" {
  description = "Should the image be vulnerability scanned on push? Default No"
  default = false
  type = bool
}
