variable "db_user" {
  description = "The user for RDS"
  default     = "postgres"
  type = string
}

variable "db_password" {
  description = "The password for RDS"
  default     = "postgres"
  type = string
}

variable "db_name" {
  description = "The database to query for in RDS"
  default     = "postgres"
  type = string
}

variable "db_url" {
  description = "The database url to connect"
  default     = "postgres"
  type = string
}

variable "db_read_endpoint" {
  description = "The database url to connect"
  default     = "postgres"
  type = string
}

variable "db_write_endpoint" {
  description = "The database url to connect"
  default     = "postgres"
  type = string
}

variable "api_token" {
  description = "The API token for the service"
  type = string
}

variable "container_repository_url" {
  description = "The URL location of the repository to pull from"
  type = string
}

variable "image_tag" {
  description = "The tag for the image to run"
  type = string
}

variable "log_group_name" {
  description = "the name of the log group to use"
  type = string
}

variable "log_group_region" {
  description = "the region of the log group to use"
  type = string
}

variable "task_execution_role_arn" {
  description = "The role to use to run the task"
  type = string
}

variable "task_role_arn" {
  description = "The role the task will use for permissions (inside container)"
  type = string
}

variable "fargate_security_group_id" {
  description = "The security group the fargate cluster (ECS) will be in"
  type = string
}

variable "alb_target_group_id" {
  description = "The security group the fargate cluster (ECS) will be in"
  type = string
}

variable "fargate_subnets" {
  description = "The subnets fargate will be in"
}

variable environment {
    description = "Is this dev or prod?"
    default = "prod"
    type = string
}

variable "ecs_cluster_name" {
  description = "The name for the fargate cluster"
  default     = "vacation-fargate-cluster"
  type = string
}