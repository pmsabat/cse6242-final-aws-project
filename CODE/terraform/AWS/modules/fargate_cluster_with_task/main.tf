# -----------------------------------------------------------------------------
# Create ECS cluster
# -----------------------------------------------------------------------------

resource "aws_ecs_cluster" "prime_cluster" {
  name = var.ecs_cluster_name
  
}

resource "aws_ecs_cluster_capacity_providers" "mixed_providers" {
  cluster_name = aws_ecs_cluster.prime_cluster.name
  capacity_providers = [ "FARGATE", "FARGATE_SPOT" ]
  default_capacity_provider_strategy {
    capacity_provider = "FARGATE_SPOT"
  }
}

# -----------------------------------------------------------------------------
# Create a task definition
# -----------------------------------------------------------------------------

locals {
  ecs_environment = [
    {
      name  = "POSTGRES_PASSWORD",
      value = "${var.db_password}"
    },
    {
      name  = "POSTGRES_USER",
      value = "${var.db_user}"
    },
    {
      name  = "POSTGRES_DB",
      value = "${var.db_name}"
    },
    {
      name  = "POSTGRES_DB_WRITE_ENDPOINT",
      value = "${var.db_write_endpoint}"
    },
    {
      name  = "POSTGRES_DB_READ_ENDPOINT",
      value = "${var.db_read_endpoint}"
    },
    {
      name  = "POSTGRES_URL",
      value = "${var.db_url}"
    },
    {
      name  = "ACCESS_TOKEN",
      value = "${var.api_token}"
    }
  ]

  ecs_container_definitions = [
    {
      image       = "${var.container_repository_url}:${var.image_tag}"
      name        = "vacation"
      networkMode = "awsvpc"

      portMappings = [
        {
          containerPort = 8000,
          hostPort      = 8000
        }
      ]

      logConfiguration = {
        logDriver = "awslogs",
        options = {
          awslogs-group         = "${var.log_group_name}",
          awslogs-region        = "${var.log_group_region}",
          awslogs-stream-prefix = "ecs"
        }
      }

      environment = flatten([local.ecs_environment])
    }
  ]
}

resource "aws_ecs_task_definition" "vacation" {
  family                   = "vacation-${var.environment}"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = "512"
  memory                   = "1024"
  execution_role_arn       = var.task_execution_role_arn
  task_role_arn            = var.task_role_arn

  container_definitions = jsonencode(local.ecs_container_definitions)
  # depends_on = [
  #   module.docker_image
  # ]
}


# -----------------------------------------------------------------------------
# Create the ECS service
# -----------------------------------------------------------------------------

resource "aws_ecs_service" "vacation" {
  name            = "vacation-service"
  cluster         = aws_ecs_cluster.prime_cluster.id
  task_definition = aws_ecs_task_definition.vacation.arn
  desired_count   = "1"

  capacity_provider_strategy {
    capacity_provider = "FARGATE_SPOT"
    weight = 2
  }

  capacity_provider_strategy {
    capacity_provider = "FARGATE"
    weight = 1
  }

  network_configuration {
    assign_public_ip = true
    security_groups  = [var.fargate_security_group_id]
    subnets          = var.fargate_subnets
  }

  load_balancer {
    target_group_arn = var.alb_target_group_id
    container_name   = "vacation"
    container_port   = "8000"
  }

  lifecycle {
    ignore_changes = [task_definition, desired_count]
  }
}

# -----------------------------------------------------------------------------
# Setup Autoscaling for fargate
# -----------------------------------------------------------------------------
resource "aws_appautoscaling_target" "ecs_target" {
  max_capacity       = 4
  min_capacity       = 1
  resource_id        = "service/${aws_ecs_cluster.prime_cluster.name}/${aws_ecs_service.vacation.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

resource "aws_appautoscaling_policy" "ecs_policy_cpu" {
  name               = "cpu-autoscaling"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.ecs_target.resource_id
  scalable_dimension = aws_appautoscaling_target.ecs_target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.ecs_target.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }

    target_value       = 60
    scale_in_cooldown  = 300
    scale_out_cooldown = 300
  }
}

resource "aws_appautoscaling_policy" "ecs_policy_memory" {
  name               = "memory-autoscaling"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.ecs_target.resource_id
  scalable_dimension = aws_appautoscaling_target.ecs_target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.ecs_target.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageMemoryUtilization"
    }

    target_value       = 80
    scale_in_cooldown  = 300
    scale_out_cooldown = 300
  }
}