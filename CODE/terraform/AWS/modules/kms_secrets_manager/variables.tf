variable "db_username" {
  description = "The username to authenticate with RDS"
  type        = string
}

variable "db_password" {
  description = "The password to authenticate with RDS"
  type        = string
}

variable "tags" {
  description = "The tags to apply to built resources"
  default = {}
  type        = map(string)
}