output "id" {
    description = "The id of the secret resource"
    value = aws_secretsmanager_secret.superuser.id
}

output "arn" {
    description = "The arn of the secret resource"
    value = aws_secretsmanager_secret.superuser.arn
}

output "version_id" {
    description = "The id for the sub-version resource"
    value = aws_secretsmanager_secret_version.superuser.id
}

output "version_arn" {
    description = "The arn for the sub-version resource"
    value = aws_secretsmanager_secret_version.superuser.arn
}

output "secret" {
    description = "The decrypted secret stored"
    value = aws_secretsmanager_secret_version.superuser.secret_string
}