variable "domain" {
  description = "The primary domain this will be hosted on"
  type = string
}

variable "subdomain" {
  description = "The sub-domain this will be hosted on"
  type = string
}

variable "target_domain_name" {
  description = "Where the CDN should forward, by domain name"
  type = string
}

variable "target_domain_id" {
  description = "Where the CDN should forward, by id"
  type = string
}

variable "acm_cert_id" {
    description = "The certificate for the CDN"
    type = string
}

variable "tag_name" {
    description = "The tag name for the CDN"
    type = string
}

variable "tag_environment" {
    description = "The tag environment for the CDN"
    type = string
}

variable "origin_request_policy_id" {
    description = "The origin request policy dictating how requests will be sent to the origin"
    type = string
    default = null
}