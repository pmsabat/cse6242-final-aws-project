resource "aws_cloudfront_distribution" "vacation_cdn" {
  origin {
    domain_name = var.target_domain_name
    origin_id   = var.target_domain_id
    custom_origin_config {
      http_port              = 80
      https_port             = 443
      origin_protocol_policy = "http-only"
      origin_ssl_protocols = [ "TLSv1.2" ]
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = "Cloudfront for our backend"

  aliases = ["${var.subdomain}.${var.domain}"]

  viewer_certificate {
    acm_certificate_arn = var.acm_cert_id
    ssl_support_method  = "sni-only"
  }

  default_cache_behavior {
    target_origin_id   = var.target_domain_id
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    viewer_protocol_policy = "redirect-to-https"
    origin_request_policy_id = var.origin_request_policy_id

    forwarded_values {
        query_string = true
        headers      = ["Origin"]

        cookies {
          forward = "none"
        }
    }
  }
  
  restrictions {
    geo_restriction {
      restriction_type = "whitelist"
      locations = ["US", "CA", "GB"]
    }
  }

  tags = {
    Environment = "${var.tag_environment}"
    Name = "Vacation"
  }
}
