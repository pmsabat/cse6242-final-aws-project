output "id" {
  description = "id of the RDS proxy"
  value       = aws_db_proxy.proxy.id
}

output "arn" {
  description = "arn of the RDS proxy"
  value       = aws_db_proxy.proxy.arn
}

output "tags" {
  description = "tags of the RDS proxy"
  value       = aws_db_proxy.proxy.tags_all
}

output "read_only_endpoint" {
  description = "endpoint for read-only connections"
  value       = aws_db_proxy_endpoint.read_only.endpoint
}

output "read_only_endpoint_id" {
  description = "id of endpoint for read-only connections"
  value       = aws_db_proxy_endpoint.read_only.id
}

output "read_only_endpoint_arn" {
  description = "arn of endpoint for read-only connections"
  value       = aws_db_proxy_endpoint.read_only.arn
}

output "read_write_endpoint" {
  description = "endpoint for read-write (mostly write, right?) connections"
  value       = aws_db_proxy_endpoint.write.endpoint
}

output "read_write_endpoint_arn" {
  description = "id of endpoint for read-write connections"
  value       = aws_db_proxy_endpoint.write.arn
}

output "read_write_endpoint_id" {
  description = "id of endpoint for read-write connections"
  value       = aws_db_proxy_endpoint.write.id
}