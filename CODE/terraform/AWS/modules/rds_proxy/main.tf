resource "aws_db_proxy" "this" {

  count = var.create_proxy ? 1 : 0

  name                   = var.name
  debug_logging          = var.debug_logging
  engine_family          = var.engine_family
  idle_client_timeout    = var.idle_client_timeout
  require_tls            = var.require_tls

  # security stuff
  # iam_role_name          = var.name
  vpc_subnet_ids         = var.vpc_subnet_ids
  vpc_security_group_ids = var.security_group_ids

  auth = {
    description = var.secret_description
    arn         = var.secret_arn
    kms_key_id  = var.secret_kms_key_id
  }

  # Target RDS instance
  target_db_cluster      = var.cluster ? true : false
  target_db_instance     = var.cluster ? false : true
  db_instance_identifier = var.cluster ? null : var.db_instance_id
  db_cluster_identifier  = var.cluster ? var.db_instance_id : null

  tags = var.tags
}

resource "aws_db_proxy_endpoint" "write" {
    count = var.cluster ? 0 : 1

    db_proxy_name          = aws_db_proxy.proxy[0].name
    db_proxy_endpoint_name = var.write_endpoint
    vpc_subnet_ids         = var.vpc_subnet_ids
    vpc_security_group_ids = var.security_group_ids
    target_role            = "READ_WRITE"

    tags = var.tags
}

resource "aws_db_proxy_endpoint" "read_only" {

    count = var.create_proxy ? 1 : 0

    db_proxy_name          = aws_db_proxy.proxy[0].name
    db_proxy_endpoint_name = var.write_endpointvar.security_group_ids
    vpc_subnet_ids         = var.vpc_subnet_ids
    vpc_security_group_ids = var.security_group_ids
    target_role            = "READ_ONLY"

    tags = var.tags
}