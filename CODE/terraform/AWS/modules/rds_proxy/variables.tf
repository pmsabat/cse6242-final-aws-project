variable name {
    description = "The name for the proxy"
    type        = string
}

variable create_proxy {
    description = "Should we create this?"
    default = true
    type = bool
}

variable debug_logging {
    description = "True/False for debug logging"
    default = false
    type = bool
}

variable engine_family {
    description = "The Engine type, PSQL or MYSQL etc., that this proxy will connect to"
    default = "POSTGRESQL"
    type = string
}

variable idle_client_timeout {
    description = "The idle time till a client is timed out"
    default = 1800
    type = number
}

variable require_tls {
    description = "Is TLS required"
    default = true
    type = bool
}

variable cluster {
    description = "Is this targetting an RDS with replicas, or an aurora cluster?  True for cluster"
    default = false
    type = bool
}

variable iam_name {
    description = "The iam name for the rds proxy to use"
    default = null
    type = string
}

# security stuff
variable vpc_subnet_ids {
    description = "The subnets this RDS proxy should be in"
    type = list
}

variable security_group_ids {
    description = "The security groups this RDS proxy should be in"
    type = list
}

variable db_username {
    description = "The username to use for DB connections"
    type = string
}

variable secret_arn {
    description = "The arn of the secret manager secret"
    type = string
}

variable secret_description {
    description = "The description of the secret manager secret"
    default = ""
    type = string
}

variable secret_kms_key_id {
    description = "The kms key id for the secrets manager"
    type = string
}

variable db_instance_id {
    description = "The RDS or Aurora instance ID - required"
    type = string
}

variable tags {
    description = "The tags to apply to resources"
    default = []
    type = list
}
