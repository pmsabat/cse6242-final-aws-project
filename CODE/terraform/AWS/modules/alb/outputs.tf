output "alb_dns_name" {
    value = aws_alb.alb.dns_name
}

output "alb_id" {
    value = aws_alb.alb.id
}

output "alb_target_group_id" {
    value = aws_alb_target_group.alb_target.id
}
