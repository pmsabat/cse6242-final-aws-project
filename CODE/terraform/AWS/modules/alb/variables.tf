variable health_check_route {
  description = "The route for the load balancer to check for health"
  default     = "/"
}

variable health_check_status_code {
  description = "The status code for the load balancer to check for health"
  default     = "200"
  type        = string
}

variable vpc_id {
  description = "The VPC ID for the ALB to exist in"
  default     = []
}

variable public_subnets {
  description = "List of public subnets ALB can exist in"
  default     = []
}

variable security_groups_for_alb {
    description = "The security groups the ALB needs to be a part of"
    default = []
}

variable alb_logging_enabled {
    description = "If we should turn ALB logging on"
    default = false
    type = bool
}

variable s3_logging_bucket_id {
    description = "The ID of the S3 bucket for the ALB to log into (only needed if logging is enabled)"
    default = null
    type = string
}
