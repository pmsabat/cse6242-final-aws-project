# -----------------------------------------------------------------------------
# Create the ALB
# -----------------------------------------------------------------------------

resource "aws_alb" "alb" {
  name            = "aws-alb"
  subnets         = var.public_subnets
  security_groups = var.security_groups_for_alb

  access_logs {
    bucket  = var.s3_logging_bucket_id
    prefix  = "alb"
    enabled = var.alb_logging_enabled
  }
}

# -----------------------------------------------------------------------------
# Create the ALB target group for ECS
# -----------------------------------------------------------------------------

resource "aws_alb_target_group" "alb_target" {
  name        = "alb-target-group-fargate"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = var.vpc_id
  target_type = "ip"

  health_check {
    path    = var.health_check_route
    matcher = var.health_check_status_code
  }
}

# -----------------------------------------------------------------------------
# Create the ALB listener
# -----------------------------------------------------------------------------

resource "aws_alb_listener" "alb_listener" {
  load_balancer_arn = aws_alb.alb.arn
  port              = "80"
  protocol          = "HTTP"
#   certificate_arn   = aws_acm_certificate.vacation.arn

  default_action {
    target_group_arn = aws_alb_target_group.alb_target.id
    type             = "forward"
  }
}
