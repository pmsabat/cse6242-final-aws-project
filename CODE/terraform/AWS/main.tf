terraform {

  # cloud {
  #   organization = "coverghoul"

  #   workspaces {
  #     tags = ["default", "cse6242"]
  #   }
  # }
  backend "s3" {
    bucket         = "tf-state-vacation-planner-cse6242"
    dynamodb_table = "terraform-state-lock-dynamo"
    key            = "vacation/state"
    region         = "us-east-1"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }

    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 2.23.1"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region = var.region
}

provider "docker" {
  host = "unix:///var/run/docker.sock"
}

# -----------------------------------------------------------------------------
# Docker Image
# -----------------------------------------------------------------------------

module "docker_image" {
  source                = "./modules/docker_container"
  container_name        = var.image_name
  container_tag         = var.image_tag
  docker_file_location  = "../"
  docker_build_platform = "linux/amd64"
  registry_address      = module.aws_ecr_repository.ecr_url
  login_address         = module.aws_ecr_repository.ecr_login_url
  registry_username     = module.aws_ecr_repository.ecr_username
  registry_password     = module.aws_ecr_repository.ecr_password
  region                = var.region
}

# -----------------------------------------------------------------------------
# VPC
# -----------------------------------------------------------------------------

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.77.0"

  name                 = "vacation"
  cidr                 = "10.0.0.0/16"
  azs                  = ["us-east-1a", "us-east-1b", "us-east-1c"]
  public_subnets       = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
  private_subnets      = ["10.0.7.0/24", "10.0.8.0/24", "10.0.9.0/24"]
  enable_dns_hostnames = true
  enable_dns_support   = true
}

# -----------------------------------------------------------------------------
# Create the Security Groups between internet, alb, fargate and RDS
# -----------------------------------------------------------------------------

# Internet to ALB - this is NOT our fargate cluster (yet)
resource "aws_security_group" "vacation_alb" {
  name        = "vacation-alb"
  description = "Allow access on port 80 only to ALB"
  vpc_id      = module.vpc.vpc_id

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# ALB TO ECS
resource "aws_security_group" "vacation_ecs" {
  name        = "vacation_ecs"
  description = "allow inbound access from the ALB only"
  vpc_id      = module.vpc.vpc_id

  ingress {
    protocol        = "tcp"
    from_port       = "80"
    to_port         = "8000"
    security_groups = [aws_security_group.vacation_alb.id]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

########################
# ALB
########################

module "alb_for_fargate" {
  source                   = "./modules/alb"
  health_check_route       = "/cat/3"
  health_check_status_code = "200"
  vpc_id                   = module.vpc.vpc_id
  public_subnets           = module.vpc.public_subnets
  security_groups_for_alb  = [aws_security_group.vacation_alb.id]
  alb_logging_enabled      = var.alb_logging_enabled
  s3_logging_bucket_id     = aws_s3_bucket.logging.id
}

########################
# RDS - aka our postgres
########################

module "db_instance" {
  identifier                    = "vacation"
  source                        = "./modules/rds"
  db_user                       = var.db_user
  db_password                   = var.db_password
  db_name                       = var.db_name
  vpc_id                        = module.vpc.vpc_id
  public_subnets                = module.vpc.public_subnets
  primary_db_security_group     = aws_security_group.vacation_ecs.id
  additional_db_security_groups = var.additional_db_security_groups
  rds_instance_size             = var.rds_instance_size
  rds_storage_size              = var.rds_storage_size
}

module "secrets_manager" {
  source      = "./modules/kms_secrets_manager"
  db_username = module.db_instance.db_user
  db_password = module.db_instance.db_password
}

module "rds_proxy" {
  source = "terraform-aws-modules/rds-proxy/aws"

  name                   = "rds-proxy"
  iam_role_name          = "rds-proxy-role"
  iam_auth               = false 
  vpc_subnet_ids         = module.vpc.public_subnets
  vpc_security_group_ids = [module.db_instance.db_security_group_id]

  db_proxy_endpoints = {
    read_write = {
      name                   = "read-write-endpoint"
      vpc_subnet_ids         = module.vpc.public_subnets
      vpc_security_group_ids = [module.db_instance.db_security_group_id]
    },
    read_only = {
      name                   = "read-only-endpoint"
      vpc_subnet_ids         = module.vpc.public_subnets
      vpc_security_group_ids = [module.db_instance.db_security_group_id]
      target_role            = "READ_ONLY"
    }
  }

  secrets = {
    "superuser" = {
      description = "PostgreSQL superuser password"
      arn         = module.secrets_manager.arn
      kms_key_id  = module.secrets_manager.id
    }
  }

  # Target Aurora cluster
  engine_family         = "POSTGRESQL"
  target_db_cluster = false
  target_db_instance = true
  db_instance_identifier = module.db_instance.identifier
  # target_db_cluster      = var.cluster ? true : false
  # target_db_instance     = var.cluster ? false : true
  # db_instance_identifier = var.cluster ? null : var.db_instance_id
  # db_cluster_identifier  = var.cluster ? var.db_instance_id : null

  tags = {
    Terraform   = "true"
    Environment = "prod"
  }
}

# module "db_proxy" {
#     source = "./modules/rds_proxy"
#     security_group_ids = [module.db_instance.db_security_group_id]
#     name = "rds_proxy"
#     vpc_subnet_ids = [module.vpc.public_subnets]
#     db_username = module.db_instance.db_user
#     secret_arn = module.secrets_manager.arn
#     secret_description = "The database username and password"
#     secret_kms_key_id = module.secrets_manager.id
#     db_instance_id = module.db_instance.id
#     # tags = []
# }

### Public Facing stuff
# -----------------------------------------------------------------------------
# Create the certificate
# -----------------------------------------------------------------------------

module "acm_certificate_and_dns" {
  source    = "./modules/certificate_and_dns"
  domain    = var.domain
  subdomain = var.subdomain
}

# -----------------------------------------------------------------------------
# Create logging
# -----------------------------------------------------------------------------

resource "aws_cloudwatch_log_group" "vacation" {
  name = "/ecs/vacation"
}

# -----------------------------------------------------------------------------
# Create IAM for logging and Fargate execution
# -----------------------------------------------------------------------------

data "aws_iam_policy_document" "vacation_log_publishing" {
  statement {
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:PutLogEventsBatch",
    ]

    resources = ["arn:aws:logs:${var.region}:*:log-group${aws_cloudwatch_log_group.vacation.name}"]
  }
}

resource "aws_iam_policy" "vacation_log_publishing" {
  name        = "vacation-log-pub"
  path        = "/"
  description = "Allow publishing to cloudwach"

  policy = data.aws_iam_policy_document.vacation_log_publishing.json
}

data "aws_iam_policy_document" "vacation_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "vacation_role" {
  name               = "vacation-role"
  path               = "/system/"
  assume_role_policy = data.aws_iam_policy_document.vacation_assume_role_policy.json
}

resource "aws_iam_role_policy_attachment" "vacation_role_log_publishing" {
  role       = aws_iam_role.vacation_role.name
  policy_arn = aws_iam_policy.vacation_log_publishing.arn
}

resource "aws_iam_role_policy_attachment" "vacation_role_fargate_task_execution" {
  role       = aws_iam_role.vacation_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

# -----------------------------------------------------------------------------
# Create an ECR for our container
# -----------------------------------------------------------------------------
module "aws_ecr_repository" {
  image_name = var.image_name
  source     = "./modules/ecr"
}

# -----------------------------------------------------------------------------
# Create the ALB log bucket
# -----------------------------------------------------------------------------
resource "aws_s3_bucket_acl" "logging" {
  acl    = "private"
  bucket = aws_s3_bucket.logging.id
}

resource "aws_s3_bucket" "logging" {
  bucket        = "vacation-${var.region}-${var.subdomain}-${var.domain}"
  force_destroy = "true"
}
# S3 Bucket for logging
data "aws_elb_service_account" "main" {
}

# -----------------------------------------------------------------------------
# Add IAM policy to allow the ALB to log to it
# -----------------------------------------------------------------------------

data "aws_iam_policy_document" "logging" {
  statement {
    actions   = ["s3:*"]
    resources = ["${aws_s3_bucket.logging.arn}/alb/*"]

    principals {
      type        = "AWS"
      identifiers = [data.aws_elb_service_account.main.arn]
    }
  }
}

resource "aws_s3_bucket_policy" "logging" {
  bucket = aws_s3_bucket.logging.id
  policy = data.aws_iam_policy_document.logging.json
}



# -----------------------------------------------------------------------------
# Create ECS cluster with our running container
# -----------------------------------------------------------------------------

module "fargate_with_task_running" {
  depends_on = [
    aws_cloudwatch_log_group.vacation,
    module.alb_for_fargate
  ]
  source                    = "./modules/fargate_cluster_with_task"
  ecs_cluster_name          = var.ecs_cluster_name
  environment               = var.environment
  db_user                   = var.db_user
  db_password               = var.db_password
  db_name                   = var.db_name
  db_url                    = module.rds_proxy.proxy_target_endpoint
  db_write_endpoint         = module.rds_proxy.proxy_target_endpoint
  db_read_endpoint          = module.rds_proxy.proxy_target_endpoint
  api_token                 = var.mapbox_token
  container_repository_url  = module.aws_ecr_repository.ecr_url
  image_tag                 = var.image_tag
  log_group_name            = aws_cloudwatch_log_group.vacation.name
  log_group_region          = var.region
  task_execution_role_arn   = aws_iam_role.vacation_role.arn
  task_role_arn             = null
  fargate_security_group_id = aws_security_group.vacation_ecs.id
  alb_target_group_id       = module.alb_for_fargate.alb_target_group_id
  fargate_subnets           = module.vpc.public_subnets
}

# -----------------------------------------------------------------------------
# Create the CDN
# -----------------------------------------------------------------------------
module "cloudfront_origin_request_policy" {
  source             = "./modules/cloudfront_origin_request_policy"
}

module "cloudfront_cdn" {
  source             = "./modules/cloudfront_dist"
  domain             = var.domain
  subdomain          = var.subdomain
  target_domain_name = module.alb_for_fargate.alb_dns_name
  target_domain_id   = module.alb_for_fargate.alb_id
  acm_cert_id        = module.acm_certificate_and_dns.acm_certificate_id
  tag_name           = "environment"
  tag_environment    = var.environment
  origin_request_policy_id = module.cloudfront_origin_request_policy.id
}
