# Install the whole project dependancies
1. Go up a directory, and run setup.sh.  Make sure it works
2. Come back here
3. Run `aws configure` with your aws account credentials 
4. Run `terraform plan` see that it complete successfully
5. In your aws account, make sure you own a domain and have a subdomain off this you're happy with
6. Now, set the variables in the variable file to your desired values, pay particular attention to the domain and subdomain variables.  These MUST be owned as they will be used for certificate issuance.
7. Run `terraform plan` again, just to be sure
8. Run `terraform apply` and carefully review the output once it displays its plan.  If you're happy with it (and this will incur monetary charges) type yes to your apply statement
9. Get a cup of coffee, this can take up to 20 minutes
10. Is it done yet? Keep waiting
11. Ok, its probably done now.  As long as it was successful, then you should be good to go, if it failed, review the output, its probably a permissions issue.

# Its deployed, now what?
1. Go ahead and get ready to push the docker container to this.
2. run `aws ecr get-login-password --region {your region} | docker login --username AWS --password-stdin {your account id}.dkr.ecr.us-east-1.amazonaws.com/vacation-prod`
    a. This logs your local docker client in with the remote AWS repository, this will be needed
3. In your AWS account, go to route 53 and create an alias record to Cloudfront for your {subdomain}.{domain}
    a. This takes a bit, so while we wait for the DNS to resolve lets push the image and start the task 
4. Build the docker container locally, test that it works with the readme from the directory above
5. Pick the image you want to push (the web container) by noting its container id from `docker images` 
6. Run this to tag your image - `docker tag {image-id} {aws_account_id}.dkr.ecr.{region}.amazonaws.com/vacation-prod:{optional-tag}`
7. Run this to push your container to aws `docker push {aws_account_id}.dkr.ecr.{region}.amazonaws.com/vacation-prod:{optional-tag}`
8. Ok, now AWS has our container, lets go ahead and start the task.  Go to `https://us-east-1.console.aws.amazon.com/ecs/home?region=us-east-1#/taskDefinitions` and check the task definition for `vacation-prod` to see it matches the image you just pushed
9. Once you've verified it to your satisfaction, check the `vacation prod` checkbox then click the `actions` button and select the dropbox option `update service`
    a. Review this page, making sure to check the box `force new deployment`.
    b. Click `skip to review`
    c. Review one more time
    d. Click `Update service`
10. Now go to `https://us-east-1.console.aws.amazon.com/ecs/home?region=us-east-1#/clusters/vacation-fargate-cluster/tasks`
    a. Make sure it launched correctly
11. You'll need to import your database data, use the snapshot provided and upload it to using the following instructions: https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/PostgreSQL.Procedural.Importing.EC2.html
11. Go to `{subdomain}.{domain}` in your browser, enjoy!
