variable "region" {
  description = "The region you wish to deploy in"
  default     = "us-east-1"
  type        = string
}

variable "additional_db_security_groups" {
  description = "List of Security Group IDs to have access to the RDS instance"
  default     = []
}


variable "db_user" {
  description = "The user for RDS"
  default     = "cse6242_user"
  type        = string
}

variable "db_password" {
  description = "The password for RDS"
  default     = "cse6242_pw"
  type        = string
}

variable "db_name" {
  description = "The database to query for in RDS"
  default     = "postgres"
  type        = string
}

variable "rds_instance_size" {
  description = "The size of RDS instance, eg db.t2.micro"
  default     = "db.t3.micro"
  type        = string
}

variable "rds_storage_size" {
  description = "The size of RDS instance's storage in GB"
  default     = 30
  type        = number
}

variable "domain" {
  description = "The primary domain this will be hosted on"
  default     = "sabs.me"
  type        = string
}

variable "subdomain" {
  description = "The sub-domain this will be hosted on"
  default     = "vacation"
  type        = string
}

variable "ecs_cluster_name" {
  description = "The name for the fargate cluster"
  default     = "vacation-fargate-cluster"
  type        = string
}

variable "mapbox_token" {
  description = "The mapbox token to use for the deployment"
  type        = string
}

variable "version_tag" {
  description = "The version you wish to deploy to"
  default     = "latest"
  type        = string
}

variable "environment" {
  description = "Is this dev or prod?"
  default     = "prod"
  type        = string
}

variable "alb_logging_enabled" {
  description = "If we should turn ALB logging on"
  default     = false
  type        = bool
}

variable "image_name" {
  description = "The image name you wish to use for ECR/docker"
  default     = "vacation-prod"
  type        = string
}

variable "image_tag" {
  description = "The image tag you wish to use for ECR/docker"
  default     = "latest"
  type        = string
}
