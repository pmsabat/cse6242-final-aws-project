variable "name" {
    description = "The name to give the container registry (and thus the container)"
    default     = "vacation-prod"
    type        = string
}

variable "db_engine" {
    description = "What type of database would you like (mysql, postgresql, redis, mongodb)"
    default     = "pg"
    type        = string
}

variable "db_version" {
    description = "What version of the provided DB type (if postgres [pg] - 13, 14, 15?)"
    default     = "14"
    type        = string
}

variable "size" {
    description = "The type of database you want per node (example: db-s-1vcpu-1gb)"
    default     = "db-s-2vcpu-4gb"
    type        = string
}

variable "region" {
    description = "The digital ocean region this should be hosted in"
    type        = string
}

variable "node_count" {
    description = "How many nodes will be in your DB cluster"
    default     = 1
    type        = number
}

variable "maintenance_window_day" {
    description = "What day do you want scheduled for maintenance"
    default     = "sunday"
    type        = number
}

variable "maintenance_window_hour" {
    description = "What hour do you want scheduled for maintenance"
    default     = "01:00"
    type        = number
}

variable "tags" {
    description = "Tags to apply to the DB cluster resource"
    default     = []
    type        = list
}

variable "private_network_uuid" {
    description = "Tags to apply to the DB cluster resource"
    default     = null
    type        = string
}

