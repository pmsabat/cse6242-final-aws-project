output "name" {
  description = "The name of the db"
  value       = digitalocean_database_cluster.this.name
}

output "id" {
  description = "The ID of the db"
  value       = digitalocean_database_cluster.this.id
}

output "host" {
  description = "The url for the db"
  value       = digitalocean_database_cluster.this.host
}

output "private_host" {
  description = "The private url for the db"
  value       = digitalocean_database_cluster.this.private_host
}

output "port" {
  description = "The port for connection for this db"
  value       = digitalocean_database_cluster.this.port
}

output "uri" {
  description = "The full uri for DB connections to the cluster"
  value       = digitalocean_database_cluster.this.uri
}

output "private_uri" {
  description = "The full private uri for DB connections to the cluster"
  value       = digitalocean_database_cluster.this.private_uri
}

output "username" {
  description = "The username for the DB"
  value       = digitalocean_database_cluster.this.user
}

output "password" {
  description = "The password for the DB"
  value       = digitalocean_database_cluster.this.password
}

output "database" {
  description = "The name of the default database for the DB"
  value       = digitalocean_database_cluster.this.database
}