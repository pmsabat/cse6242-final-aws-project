terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  } 
}

provider "digitalocean" {
}


resource "digitalocean_database_cluster" "this" {
    name       = var.name
    engine     = var.db_engine
    version    = var.db_version
    size       = var.size
    region     = var.region
    node_count = var.node_count

    maintenance_window {
      day  = var.maintenance_window_day
      hour = var.maintenance_window_hour
    }

    tags = var.tags

    private_network_uuid = var.private_network_uuid
}