output "name" {
  description = "The name of the container registry"
  value       = digitalocean_container_registry.this.name
}

output "id" {
  description = "The ID of the container registry"
  value       = digitalocean_container_registry.this.id
}

output "endpoint" {
  description = "The url for the registry"
  value       = digitalocean_container_registry.this.endpoint
}

output "subscription_tier_slug" {
  description = "The subscription tier for this resource"
  value       = digitalocean_container_registry.this.subscription_tier_slug
}

output "created_at" {
  description = "When was this resource created"
  value       = digitalocean_container_registry.this.created_at
}

output "auth_credentials" {
  description = "The authorization string for the docker daemon to this resource"
  value       = digitalocean_container_registry_docker_credentials.this.docker_credentials
}

output "auth_credentials" {
  description = "The authorization string for the docker daemon to this resource"
  value       = digitalocean_container_registry_docker_credentials.this.docker_credentials
}

output "credentials_expiration_time" {
  description = "The time the above credentials expire"
  value       = digitalocean_container_registry_docker_credentials.this.credential_expiration_time
}