terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  } 
}

provider "digitalocean" {
}


resource "digitalocean_container_registry" "this" {
  name                   = var.name
  subscription_tier_slug = var.sub_tier
  region                 = var.region
}

resource "digitalocean_container_registry_docker_credentials" "this" {
  registry_name = digitalocean_container_registry.this.name
}

