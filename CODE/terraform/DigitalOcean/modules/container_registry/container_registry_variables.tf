variable "name" {
    description = "The name to give the container registry (and thus the container)"
    default     = "vacation-prod"
    type        = string
}

variable "sub_tier" {
    description = "The type of container registry subscription you want (starter: $0 = 50MB, basic: $5 = 5GB, professional: $20 = 100GB)"
    default     = "basic"
    type        = string
}

variable "region" {
    description = "The digital ocean region this should be hosted in"
    type        = string
}
