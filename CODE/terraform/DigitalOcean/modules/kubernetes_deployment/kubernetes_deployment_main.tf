data "digitalocean_kubernetes_cluster" "this" {
  name = var.do_k8s_cluster_name
}

kubernetes = {
    source  = "hashicorp/kubernetes"
    version = ">= 2.0.0"   
    host    = data.digitalocean_kubernetes_cluster.this.endpoint
    token   = data.digitalocean_kubernetes_cluster.this.kube_config[0].token
    cluster_ca_certificate = base64decode(
    data.digitalocean_kubernetes_cluster.example.kube_config[0].cluster_ca_certificate
  )
}

resource "kubernetes_deployment" "this" {
  metadata {
    name = var.app_name
  }

  spec {
    replicas = 2

    selector {
      match_labels = {
        app = var.app_name
      }
    }

    template {
      metadata {
        labels = {
          app = var.app_name
        }
      }

      spec {
        container {
          name  = var.container_name
          image = var.container_image
          # ports {
          #   container_port = var.container_port
          # }
        }

        resources {
          limits = {
            cpu    = var.cpu_limit
            memory = var.memory_limit
          }
          requests = {
            cpu    = var.cpu_request
            memory = var.memory_limit
          }
        }
      }

      liveness_probe {
        http_get {
          path = var.health_check_endpoint
          port = var.health_check_port

          http_header {
            name  = var.health_check_header_name
            value = var.health_check_header_value
          }
        }

        initial_delay_seconds = var.health_check_delay_in_seconds
        period_seconds        = var.health_check_period_in_seconds
      }
    }
  }
}
