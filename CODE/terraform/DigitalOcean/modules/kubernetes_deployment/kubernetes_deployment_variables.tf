variable "app_name" {
    description = "The app name to give this deployment registry"
    type        = string
}

variable "do_k8s_cluster_name" {
    description = "The DO clusters name, must be passed"
    type        = string
}

variable "container_name" {
    description = "The DNS name of the container for the k8s cluster"
    type        = string
}

variable "container_image" {
    description = "What image to pull and run [name]:[version]?"
    type        = string
}

variable "health_check_port" {
    description = "The port for the healthcheck to run on"
    type        = number
}

variable "health_check_endpoint" {
    description = "The /path for the healthcheck to run on"
    type        = string
}

variable "health_check_header_name" {
    description = "The health check header name to be set on requests"
    type        = string
}

variable "health_check_header_value" {
    description = "The health check header value to be set on requests"
    type        = string
}

variable "health_check_delay_in_seconds" {
    description = "How long to wait before starting health check requests"
    type        = number
}

variable "health_check_period_in_seconds" {
    description = "How long to wait between starting health check requests"
    type        = number
}

variable "cpu_limit" {
    description = "The limit per container for CPU resource consumption"
    type        = string
}

variable "memory_limit" {
    description = "The limit per container for memory resource consumption"
    type        = string
}

variable "cpu_request" {
    description = "The CPU resources to be requested to run the container (min)"
    type        = string
}

variable "memory_request" {
    description = "The memory resources to be requested to run the container (min)"
    type        = string
}
