variable "name" {
    description = "The name to give the container registry (and thus the container)"
    default     = "kubernetes-prod"
    type        = string
}

variable "ha" {
    description = "Should this use an HA control plane, only changable at create time"
    default     = true
    type        = bool
}

variable "auto_upgrade" {
    description = "Should this k8s cluster auto-upgrade"
    default     = true
    type        = bool
}

variable "k8_version" {
    description = "What version of DO k8s?"
    default     = "1.21.2-do.2"
    type        = string
}

variable "size" {
    description = "The type of VM do you want per node (example: s-1vcpu-2gb)"
    default     = "s-2vcpu-4gb"
    type        = string
}

variable "region" {
    description = "The digital ocean region this should be hosted in"
    type        = string
}

variable "node_count" {
    description = "How many nodes will be in your k8s cluster"
    default     = 1
    type        = number
}

variable "maintenance_window_day" {
    description = "What day do you want scheduled for maintenance"
    default     = "sunday"
    type        = string
}

variable "maintenance_window_hour" {
    description = "What hour do you want scheduled for maintenance"
    default     = "01:00"
    type        = string
}

variable "tags" {
    description = "Tags to apply to the k8s cluster resource"
    default     = []
    type        = list
}

variable "vpc_uuid" {
    description = "VPC to apply to the k8S cluster resource"
    default     = null
    type        = string
}

