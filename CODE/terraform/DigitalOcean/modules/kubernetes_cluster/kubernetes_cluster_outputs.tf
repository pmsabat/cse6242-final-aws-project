output "name" {
  description = "The name of the k8s cluster"
  value       = digitalocean_kubernetes_cluster.this.name
}

output "id" {
  description = "The ID of the k8s cluster"
  value       = digitalocean_kubernetes_cluster.this.id
}

output "urn" {
  description = "The urn for the k8s cluster"
  value       = digitalocean_kubernetes_cluster.this.urn
}

output "endpoint" {
  description = "The private url for the k8s"
  value       = digitalocean_kubernetes_cluster.this.endpoint
}

output "cluster_subnet" {
  description = "The subnet the cluster is in"
  value       = digitalocean_kubernetes_cluster.this.cluster_subnet
}

output "services_subnet" {
  description = "The subnet for services running on this k8s deployment"
  value       = digitalocean_kubernetes_cluster.this.service_subnet
}