provider "digitalocean" {
}

resource "digitalocean_kubernetes_cluster" "this" {
  name         = var.name
  region       = var.region
  version      = var.k8_version
  vpc_uuid     = var.private_network_uuid
  ha           = var.ha
  auto_upgrade = var.auto_upgrade
  tags         = var.tags

  node_pool {
    name       = "default"
    size       = var.size
    node_count = 2

    autoscaling {
      min_nodes = 2
      max_nodes = 10
    }
  }

  maintenance_policy {
    start_time  = var.maintenance_window_hour
    day         = var.maintenance_window_day
  }
}