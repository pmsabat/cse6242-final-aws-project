#!/bin/bash
echo 'starting asdf timer'
if [[ $OSTYPE == 'darwin'* ]]; then
  echo 'macOS detected, installing asdf'
  brew tap hashicorp/tap
  brew install asdf gpg gawk libpq gdal hashicorp/tap/terraform
  echo -e "\n. $(brew --prefix asdf)/libexec/asdf.sh" >> ${ZDOTDIR:-~}/.zshrc
  curl "https://awscli.amazonaws.com/AWSCLIV2.pkg" -o "AWSCLIV2.pkg"
  sudo installer -pkg AWSCLIV2.pkg -target /
  # source ~/.zshrc
else
  echo 'linux detected, installing asdf'
  sudo apt install -y curl git coreutils dirmngr gpg curl gawk libpq-dev libgdal-dev gdal-bin gnupg software-properties-common
  wget -O- https://apt.releases.hashicorp.com/gpg | \
    gpg --dearmor | \
    sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
  echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] \
    https://apt.releases.hashicorp.com $(lsb_release -cs) main" | \
    sudo tee /etc/apt/sources.list.d/hashicorp.list
  sudo apt update && sudo apt-get install terraform -y
  git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.10.2
  echo -e "\n. $HOME/.asdf/asdf.sh"
  echo -e "\n. $HOME/.asdf/completions/asdf.bash"
  curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
  unzip awscliv2.zip
  sudo ./aws/install
fi
asdf plugin add python
asdf install python 3.10.6
echo 'done asdf install'
if [[ "$(python -V)" =~ "Python 3" ]]; then
    curl -sSL https://install.python-poetry.org | python -

else
    curl -sSL https://install.python-poetry.org | python3 -
fi
poetry install
poetry run pre-commit install
export PATH="$HOME/.local/bin:$PATH"
