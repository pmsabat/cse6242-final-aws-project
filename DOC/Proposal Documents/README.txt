Airbnb Optimized: A Personalized and Improved Travel Planner 
USER GUIDE 

DESCRIPTION  
To streamline, optimize collaboration and deploy the vacation planning tool, we established a Docker container with applications such as JupyterLab, PostgreSQL  

JupyterLab is web based interactive framework for developing and running notebooks. In our application, we have developed notebooks that help in acquiring the data required for the application. We have a notebook for Airbnb, which downloads static data and writes to the database. We acquired Content API keys for TripAdvisor which were paid and with a limited validity. We collected the data using these API keys and saved it to the database. We have used PostgreSQL inside our docker containers as it's very stable and simple to use.  

The web application is containerized and hosted on AWS. See https://vacation.sabs.me for the live demo! 

INSTALLATION  
Following are the steps for setting up on Linux: 
1) RUN ./setup.sh 
2) Install docker & docker-compose for your platform: https://docs.docker.com/get-docker/ 
3) Go to mapbox.com and sign up for a free account, replace the token variable in the .env file with your MapBox API token.  This can bill you if you perform more than 100,000 requests in a month (each use of our project takes ~5 api calls). 
4) RUN docker-compose up --build –d It forces it to rebuild but uses the docker cache for speed. The –d parameter disconnects from the terminal. 
5) Load the data by following the steps below. 
6) Go to analysis.localhost/ to locally access the web application. 

To bypass precommit hooks use the -n flag Otherwise, realize the black styling requires you to commit, then after it styles commit again. For flake8, this should occur at the same time as black For mypy, this is a typing module for python to prevent type conflicts 

Loading The Data 
Following are the steps to load the application, we recommend restoring from the pg_dumb backup, but feel free to run the notebooks with your API keys if desired: 
1) Restore the data into database from backup dump. 
	a. Untar db_dump.tar.gz  and copy the back up in the postgres container docker cp <data dump file> <container id>:<path> 
	b. Restore in postgres container psql  <database URL> < data dump file (Refer to the environment file for database URL) 

2) If you have the TripAdvisor API keys for the services, add those to the TripAd_Restaurant.ipynb  and Trip_Ad.ipynb in the notebooks folder and execute the notebooks. To acquire the API keys follow steps on https://developer-tripadvisor.com/content-api/.  Upon getting API keys run the notebooks in the following order: 
	a. Trip_ad.ipynb 
	b. TripAd_Restaurants.ipynb 
	c. cleandata_csv.ipynb 

The web application is hosted on AWS and is available https://vacation.sabs.me for a live demo. For AWS Deployment, please refer to the detailed steps in the readme.md file in the terraform folder.  Note, you will need your own aws account and domain before you start. 

EXECUTION  
Once the web application is up and running, following steps can be carried out: 
1) The left panel on the web page takes in the User preferences as input and they can select from multiple drop-down options.  
	- Accommodation Type  
	- Minimum Cost per Night 
	- Maximum Cost per Night 
	- Number of people
	- Number of Nights 
	- What do you like to do? 
	- What do you like to eat? 
	- Maximum Food Price 
	- How much is your time worth? ($/hr) 
Here How much is your time worth? tells how much more would one be willing to pay to stay 	closer to desired attractions/restaurants. 
2) Next, click Submit to display the different Airbnbs (blue), Attractions (orange) and Restaurants(green) on the map and compute the TSP route calculation. 
3) Single click on any plotted location, shown on the map, will bring up a tooltip which shows its details such as the name, price, reviews/ratings, URL. The Airbnb tooltip also shows the complete route's information. 
4) Hovering over any Airbnb location will highlight the route that goes through the Airbnb and the closest Attractions and Restaurants that meet the user input. 

DEMO VIDEO URL 
https://www.youtube.com/watch?v=jQRHX0hNbgI 
